# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/h0las/dg_amgx/CSR.cpp" "/home/h0las/dg_amgx/cmake-build-debug/CMakeFiles/dg_amgx.dir/CSR.cpp.o"
  "/home/h0las/dg_amgx/MatrixSolver.cpp" "/home/h0las/dg_amgx/cmake-build-debug/CMakeFiles/dg_amgx.dir/MatrixSolver.cpp.o"
  "/home/h0las/dg_amgx/SolverHypre.cpp" "/home/h0las/dg_amgx/cmake-build-debug/CMakeFiles/dg_amgx.dir/SolverHypre.cpp.o"
  "/home/h0las/dg_amgx/SolverHypreGmres.cpp" "/home/h0las/dg_amgx/cmake-build-debug/CMakeFiles/dg_amgx.dir/SolverHypreGmres.cpp.o"
  "/home/h0las/dg_amgx/gasdin2d.cpp" "/home/h0las/dg_amgx/cmake-build-debug/CMakeFiles/dg_amgx.dir/gasdin2d.cpp.o"
  "/home/h0las/dg_amgx/global.cpp" "/home/h0las/dg_amgx/cmake-build-debug/CMakeFiles/dg_amgx.dir/global.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/lib/x86_64-linux-gnu/openmpi/include/openmpi"
  "/usr/lib/x86_64-linux-gnu/openmpi/include/openmpi/opal/mca/event/libevent2022/libevent"
  "/usr/lib/x86_64-linux-gnu/openmpi/include/openmpi/opal/mca/event/libevent2022/libevent/include"
  "/usr/lib/x86_64-linux-gnu/openmpi/include"
  "../contrib/AMGX/base/include"
  "../contrib/hypre/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
