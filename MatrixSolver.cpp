#include "MatrixSolver.h"
#include "global.h"
#include "SolverHypreGmres.h"
#include "stdio.h"

MatrixSolver* MatrixSolver::create(const char* solverName)
{
	if (strcmp(solverName, "HYPRE_GMRES") == 0) {
		return new SolverHypreGmres();
	}
    else {
        printf("Error: can't create hypre solver!!!\n");    
    }
}

void MatrixSolver::init(int cellsCount, int blockDimension)
{
	blockDim = blockDimension;
	int n = cellsCount*blockDim;
	a = new CSRMatrix(n);
	b = new double[n];
	x = new double[n];
}

void MatrixSolver::zero() {
	memset(x, 0, sizeof(double)*a->n);
	memset(b, 0, sizeof(double)*a->n);
	a->zero();
}

MatrixSolver::~MatrixSolver()
{
	delete a;
	delete[] b;
	delete[] x;
}

void MatrixSolver::setMatrElement(int i, int j, double value)
{
    a->set(i, j, value);
}

void MatrixSolver::setRightElement(int i, double value)
{
	b[i] = value;
}


void MatrixSolver::addMatrElement(int i, int j, double value)
{
	a->add(i, j, value);
}

void MatrixSolver::createMatrElement(int i, int j) 
{
	a->init(i, j);
}

void MatrixSolver::initCSR()
{
	a->assemble();
}

void MatrixSolver::addRightElement(int i, double value)
{
	b[i] += value;
}

void MatrixSolver::printToFile(const char* fileName)
{
	a->printToFile(fileName);
}


int SolverJacobi::solve(double eps, int& maxIter)
{
	if (!tempXAlloc) {
		tempX = new double [a->n];
		tempXAlloc = true;
	}
	double	aii;
	double	err = 1.0;
	int		step = 0;
	double	tmp;
	//memset(x, 0, sizeof(double)*a->n);
	while(err > eps && step < maxIter)
	{
		step++;
		for (int i = 0; i < a->n; i++)
		{
			tmp = 0.0;
			aii = 0;
			for (int k = a->ia[i]; k < a->ia[i+1]; k++)
			{
				if (i == a->ja[k])	// i == j
				{
					aii = a->a[k];
				} else {
					tmp += a->a[k]*x[a->ja[k]];
				}
			}
			if (fabs(aii) <= eps*eps) 
			{
				printf("JACOBI_SOLVER: error: a[%d, %d] = 0\n", i, i);
				return MatrixSolver::RESULT_ERR_ZERO_DIAG;
			}
			//x[i] = (-tmp+b[i])/aii;
			tempX[i] = (-tmp+b[i])/aii;
		}
		err = 0.0;
		for (int i = 0; i < a->n; i++)
		{
			tmp = 0.0;
			for (int k = a->ia[i]; k < a->ia[i+1]; k++)
			{
				x[a->ja[k]] = tempX[a->ja[k]];
				tmp += a->a[k]*x[a->ja[k]];
			}
			err += fabs(tmp-b[i]);
		}
		int qqqqq = 0; // ZHRV_WARN
	}
	if (step >= maxIter)
	{
		printf("JACOBI_SOLVER: (warning) maximum iterations done (%d); error: %e\n", step, err);
		maxIter = step;
		return MatrixSolver::RESULT_ERR_MAX_ITER;
	}
	maxIter = step;
	return MatrixSolver::RESULT_OK;
}


