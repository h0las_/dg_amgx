#include "SolverHypre.h"

void SolverHypre::initMatrVectors()
{
	HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, ilower, iupper, &A);
	HYPRE_IJMatrixSetObjectType(A, HYPRE_PARCSR);
	HYPRE_IJMatrixInitialize(A);

	/* Create the rhs and solution */
	HYPRE_IJVectorCreate(MPI_COMM_WORLD, ilower, iupper, &bb);
	HYPRE_IJVectorSetObjectType(bb, HYPRE_PARCSR);
	HYPRE_IJVectorInitialize(bb);

	HYPRE_IJVectorCreate(MPI_COMM_WORLD, ilower, iupper, &xx);
	HYPRE_IJVectorSetObjectType(xx, HYPRE_PARCSR);
	HYPRE_IJVectorInitialize(xx);
}

void SolverHypre::init(int cellsCount, int blockDimension)
{
	blockDim = blockDimension;
	HYPRE_Int n = cellsCount*blockDim;
	ilower = 0;
	iupper = n - 1;
	local_size = iupper - ilower + 1;

	cols	= new HYPRE_Int[blockDim];
	values	= new double[n];
	x		= new double[n];

	initMatrVectors();

}

void SolverHypre::zero() {
	HYPRE_IJMatrixDestroy(A);
	HYPRE_IJVectorDestroy(bb);
	HYPRE_IJVectorDestroy(xx);

	HYPRE_IJMatrixCreate(MPI_COMM_WORLD, ilower, iupper, ilower, iupper, &A);
	HYPRE_IJMatrixSetObjectType(A, HYPRE_PARCSR);
	HYPRE_IJMatrixInitialize(A);

	/* Create the rhs and solution */
	HYPRE_IJVectorCreate(MPI_COMM_WORLD, ilower, iupper, &bb);
	HYPRE_IJVectorSetObjectType(bb, HYPRE_PARCSR);
	HYPRE_IJVectorInitialize(bb);

	HYPRE_IJVectorCreate(MPI_COMM_WORLD, ilower, iupper, &xx);
	HYPRE_IJVectorSetObjectType(xx, HYPRE_PARCSR);
	HYPRE_IJVectorInitialize(xx);

	memset(x, 0, local_size*sizeof(double));
}

SolverHypre::~SolverHypre()
{
	HYPRE_IJMatrixDestroy(A);
	HYPRE_IJVectorDestroy(bb);
	HYPRE_IJVectorDestroy(xx);
	delete[] x;
}

void SolverHypre::setMatrElement(int i, int j, double value) {
	HYPRE_Int nrow = 1;
	HYPRE_Int ncol = 1;

	HYPRE_Int row = i;
	HYPRE_Int col = j;

	HYPRE_IJMatrixSetValues(A, nrow, &ncol, &row, &col, &value);
}

void SolverHypre::addMatrElement(int i, int j, double value)
{
	HYPRE_Int nrow = 1;
    HYPRE_Int ncol = 1;    
	
    HYPRE_Int row = i;
    HYPRE_Int col = j;

	HYPRE_IJMatrixAddToValues(A, nrow, &ncol, &row, &col, &value);
}


void SolverHypre::setRightElement(int i, double value)
{
	HYPRE_Int nrow = 1;
    HYPRE_Int row = i;

	HYPRE_IJVectorSetValues(bb, nrow, &row, &value);
}


void SolverHypre::addRightElement(int i, double value)
{
	HYPRE_Int nrow = 1;
    HYPRE_Int row = i;

	HYPRE_IJVectorAddToValues(bb, nrow, &row, &value);
}

void SolverHypre::setParameter(const char* name, int val)
{
	if (strcmp(name, "PRINT_LEVEL") == 0) {
		PRINT_LEVEL = val;
	}
}


void SolverHypre::printToFile(const char* fileName)
{
	HYPRE_Int n = local_size;
	HYPRE_Int nc = 1;
	double * x = new double[n];
	HYPRE_Int * cols = new HYPRE_Int[n];
	FILE * fp = fopen(fileName, "w");
	for (int i = 0; i < n; i++) {
		cols[i] = ilower + i;
	}

	for (HYPRE_Int row = 0; row < n; row++) {
		HYPRE_IJMatrixGetValues(A, 1, &nc, &row, &row, x);
		for (HYPRE_Int i = 0; i < nc; i++) {
			fprintf(fp, "%25.16e  ", x[i]);
		}
		fprintf(fp, "\n");
	}

	fprintf(fp, "\n\n=============================================================================================\n\n\n");
	HYPRE_IJVectorGetValues(xx, local_size, cols, x);
	for (int i = 0; i < n; i++) {
		fprintf(fp, "%25.16e  ", x[i]);
	}

	fclose(fp);
	delete[] x;
	delete[] cols;
}

