﻿#include "CSR.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <cmath>
#include <stdarg.h>
#include <float.h>
#include "gasdin2d.h"
#include "time.h"
#include <amgx_c.h>
#include <amgx_config.h>
#include "MatrixSolver.h"
#include <mpi.h>

const int qxBlock = 0;
const int qyBlock = 1;
const int uBlock = 2;

const int blocksCount = 3;

struct Point
{
	double x, y;
};

enum DATA_LAYER
{
	OLD = 0,
	NEW = 1
};

#define Vector Point
#define _min_ min
#define _max_ max

const char		TASK_NAME[50] = "test";

double	EPS = 1.0e-6;

const double	TMAX = 0.1;
const double	TAU = 1.e-4;

const int		SAVE_STEP = 100;

const int NX = 4;
const int NY = 4;

const int countNodesInCell = 3;

const double XMIN = 0.0;
const double XMAX = 1.0;
const double YMIN = 0.0;
const double YMAX = 1.0;

const double HX = (XMAX - XMIN) / NX;
const double HY = (YMAX - YMIN) / NY;

int			nodesCount;
Point	*	nodes;

int			cellsCount;
int		**	cellNodes;
int		**	cellNeigh;
int		**	cellEdges;
int		*	cellType;
double	*	cellS;
float  *   cellCx;
float  *   cellCy;
bool	*	cellIsBound;

int			edgesCount;
int		*	edgeNode1;
int		*	edgeNode2;
Vector	*	edgeNormal;
double	*	edgeL;
int		*	edgeCell1;
int		*	edgeCell2;
int		*	edgeType;
Point	*	edgeC;

float		*	cellDXx;
float		*	cellDXy;

double		*u, *wx, *wy;
double		*uOld, *wxOld, *wyOld;
float		*fU, *fWx, *fWy;
float		*fUOld, *fWxOld, *fWyOld;

double		resU, resWx, resWy;
double	    *intU, *intWx, *intWy;
double      **intFi;

int funcCount = 3;
float		*cellA, *cellInvA;
float		*matrA, *matrInvA;

Point		**edgeGP;
float		*cellGPx;
float		*cellGPy;
float		*edgeWGP, *edgeJ;
float		*cellWGP, *cellJ;
int			edgeGPCount, cellGPCount;

FILE	*	fileLog;

int step;
double t;

float	*b;
int A_size;
const int A_block_size = 9;
const int A_small_block_size = 3;

int solve(double eps, int maxIter, CSRMatrix	*a, float *b, float *x);
double exact(double t, double x, double y);
void runExplicitSolver();
void runAmgxSolver();
void runHypreSolver();
float GetFuncComposition(int iCell1, int iCell2, int iFunc1, int iFunc2, int iEdge, int iGP);
void AddMatrElement(CSRMatrix& matr, int iCell, int iBlock, int iTargetBlock, int iBlockRow, int iBlockCol, float value);
void AddFluxMatrElement(CSRMatrix& matr, int iCell1, int iCell2, int iEdge, int iBlock, bool isBorder, int sign);
void AddMatrElement_flux(CSRMatrix& matr, int iCell1, int iCell2, int iBlock, int iTargetBlock, int iBlockRow, int iBlockCol, float value);
void SetMatrElement(CSRMatrix& matr, int iCell, int iBlock, int iTargetBlock, int iBlockRow, int iBlockCol, float value);
void initRightPart(float *lastLayerSolution, float *rightPart_data);
void initRightPart_hypre(MatrixSolver *hypreSolver);
void initMatrix(CSRMatrix& A);
void initMatrix_hypre(MatrixSolver *hypreSolver);
void initMatrix_convection(CSRMatrix& A);
void loadGrid(char* fileName);
void initGrid();
void destroyGrid();
void initData();
void destroyData();
void calcIntegralSurf();
void calcIntegralVol();
void calcNewValues();
void calcGradientsVolIntegral();
void calcGradientsSurfIntegral();
void calcNewGradientValues();
void save(int);
void writeVTK(char*, int);
void writeVTK_AMGX(char*, int, float *currentLayerSolution);

float get_vx(int iCell);
float get_vy(int iCell);
float get_dx(int iCell1, int iCell2);
float get_dx_boundary(int iCell1, int iEdge);
float get_boundary(int iCell, int iEdge);

double	baseF(int, int, double, double);
double	baseDFDx(int, int, double, double);
double	baseDFDy(int, int, double, double);
void	getFields(double&, double&, double&, int, double, double, DATA_LAYER);
void getFlux(double& FU, double& FWx, double& FWy,
	double Ul, double Wxl, double Wyl,
	double Ur, double Wxr, double Wyr, Vector n, bool isBoundary);
void calcMatr(int iCell);
void calcCellGP(int iCell);
void calcEdgeGP(int iEdge);
void cellAddNeigh(int c1, int c2);
void GetK(double&, double&, double&, double&);
void copyToOld();
void copyToOld(float *lastLayerSolution, float *currentLayerSolution, int N);
void inverseMatr(float* a_src, float *am, int N);
void inverseMatr__(double** a_src, double **am, int N);
void OutPut(const char* str, ...);
int __getEdgeByCells(int c1, int c2);

double max(double, double);
double min(double, double);
double max(double, double, double);
double min(double, double, double);
inline double zero(double a) { return (abs(a) < EPS) ? 0.0 : a; }
inline double pow_2(double x) { return x*x; }
inline double pow_3(double x) { return x*x*x; }
inline double pow_4(double x) { return x*x*x*x; }

int main(int argc, char **argv)
{
#ifdef _DEBUG
	_controlfp(~(_MCW_EM & (~_EM_INEXACT) & (~_EM_UNDERFLOW)), _MCW_EM);
#endif

	srand(time(0));

	//fileLog = fopen("galerkin2d.log", ".w");

	//loadGrid("square.1");

	initGrid();

	initData();

	t = 0.0;
	step = 0;

	// ****************** Run solver here ************************
	//runExplicitSolver();
	runAmgxSolver();

	/*MPI_Init(&argc, &argv);

	runHypreSolver();

	MPI_Finalize();*/

	destroyData();

	destroyGrid();

	//fclose(fileLog);

	return 0;
}

int solve(double eps, int maxIter, CSRMatrix	*a, float *b, float *x)
{
	double	aii;
	double	err = 1.0;
	int		step = 0;
	double	tmp;

	memset(x, 0, sizeof(double)*a->n);
	while (err > eps && step < maxIter)
	{
		step++;
		for (int i = 0; i < a->n; i++)
		{
			tmp = 0.0;
			aii = 0;
			for (int k = a->ia[i]; k < a->ia[i+1]; k++) {
				if (a->ja[k] == i) {
					aii = a->a[k];
				}
				else {
					tmp += a->a[k] * x[a->ja[k]];
				}
			}
			if (fabs(aii) <= eps*eps)
			{
				//log((char*)"SEIDEL_SOLVER: error: a[%d, %d] = 0\n", i, i);
				printf("Errror eps*eps");
				return -1;
			}
			x[i] = (-tmp + b[i]) / aii;
		}
		err = 0.0;
		for (int i = 0; i < a->n; i++)
		{
			tmp = 0.0;
			for (int k = a->ia[i]; k < a->ia[i + 1]; k++) {
				tmp += a->a[k] * x[a->ja[k]];
			}
			err += fabs(tmp - b[i]);
		}
		//int qqqqq = 0; // ZHRV_WARN
		//printf("SEIDEL SOLVER: step = %5d\terr = %16.8e\n", step, err);
	}
	if (step >= maxIter)
	{
		//log((char*)"SEIDEL_SOLVER: (warning) maximum iterations done (%d); error: %e\n", step, err);
		maxIter = step;
		//return MatrixSolver::RESULT_ERR_MAX_ITER;
		printf("Error step");
		return -1;
	}

	maxIter = step;

	return 0;
}

void runExplicitSolver()
{
	copyToOld();
	writeVTK((char*)TASK_NAME, 0);

	while (t < TMAX) {
		t += TAU;
		step++;

		memset(intU, 0.0, sizeof(double) * cellsCount * funcCount);
		memset(intWx, 0.0, sizeof(double) * cellsCount * funcCount);
		memset(intWy, 0.0, sizeof(double) * cellsCount * funcCount);

		calcGradientsVolIntegral();

		calcGradientsSurfIntegral();

		calcNewGradientValues();

		calcIntegralVol();

		calcIntegralSurf();

		calcNewValues();

		copyToOld();

		if (step % SAVE_STEP == 0)
		{
		    writeVTK((char*)TASK_NAME, step);

			double L2 = 0.0;

			for (int i = 0; i < cellsCount; i++){
				L2 += cellS[i]*(fU[i * funcCount + 0] - exact(t, cellCx[i], cellCy[i]))*(fU[i * funcCount + 0] - exact(t, cellCx[i], cellCy[i]));
			}

			OutPut("L2 = %15.10f\n", sqrt(L2));
		}
		if (step % 100 == 0)
		{
			OutPut("%8d | time: %15.8e | U: %15.8e | WX: %15.8e | WY: %15.8e |\n", step, t, resU, resWx, resWy);
		}
		if(step == 700)
		{
			printf("runtime = %f", (clock() / 1000.0));
		}
	}
}

void runHypreSolver()
{
    MatrixSolver *hypreSolver = MatrixSolver::create("HYPRE_GMRES");

    hypreSolver->init(cellsCount, A_block_size);
    hypreSolver->zero();

    for (int iCell = 0; iCell < cellsCount; iCell++) {
        hypreSolver->x[A_block_size * iCell + 6] = sin(cellCx[iCell] * M_PI) * sin(cellCy[iCell] * M_PI);
    }

    initRightPart_hypre(hypreSolver);

    initMatrix_hypre(hypreSolver);

    hypreSolver->printToFile("hypre_matrix.txt");

    int maxIter = 50;

    //writeVTK_AMGX((char*)TASK_NAME, step, hypreSolver->x);

    while (t < TMAX) {
        t += TAU;
        step++;

        // ************ begin hypre solver ************

        hypreSolver->solve(EPS, maxIter);

        initRightPart_hypre(hypreSolver);

        if (step % SAVE_STEP == 0)
        {
            //writeVTK_AMGX((char*)TASK_NAME, step, hypreSolver->x);

            double L2 = 0.0;

            for (int i = 0; i < cellsCount; i++){
                L2 += cellS[i]*(hypreSolver->x[i * A_block_size + 6] - exact(t, cellCx[i], cellCy[i]))*(hypreSolver->x[i * A_block_size + 6] - exact(t, cellCx[i], cellCy[i]));
            }

            OutPut("L2 = %15.10f\n", sqrt(L2));
        }
        if (step % 1 == 0)
        {
            OutPut("%8d | time: %15.8e | U: %15.8e | WX: %15.8e | WY: %15.8e |\n", step, t, resU, resWx, resWy);
        }

        if(step == 700)
        {
            printf("runtime = %f", (clock() / 1000.0));
        }

        memset(hypreSolver->x, 0.0, sizeof(double) * A_block_size * cellsCount);
    }
}

void runAmgxSolver()
{
	float *b_data = new float[A_block_size * cellsCount];
	float *lastLayerSolution = new float[A_block_size * cellsCount];
	float *currentLayerSolution = new float[A_block_size * cellsCount];

	memset(b_data, 0.0, sizeof(float) * A_block_size * cellsCount);
	memset(lastLayerSolution, 0.0, sizeof(float) * A_block_size * cellsCount);
	memset(currentLayerSolution, 0.0, sizeof(float) * A_block_size * cellsCount);

	for (int iCell = 0; iCell < cellsCount; iCell++) {
		lastLayerSolution[A_block_size * iCell + 6] = sin(cellCx[iCell] * M_PI) * sin(cellCy[iCell] * M_PI);
	}

	// begin AMGX init
	AMGX_initialize();
	AMGX_initialize_plugins();

	AMGX_config_handle config;
	AMGX_config_create_from_file(&config, "configs/config.json");

	AMGX_resources_handle rsrc;
	AMGX_resources_create_simple(&rsrc, config);

	AMGX_solver_handle solver;
	AMGX_matrix_handle A_amgx;
	AMGX_vector_handle b_amgx;
	AMGX_vector_handle solution_amgx;

	AMGX_solver_create(&solver, rsrc, AMGX_mode_dFFI, config);
	AMGX_matrix_create(&A_amgx, rsrc, AMGX_mode_dFFI);
	AMGX_vector_create(&b_amgx, rsrc, AMGX_mode_dFFI);
	AMGX_vector_create(&solution_amgx, rsrc, AMGX_mode_dFFI);
	// end AMGX init

	CSRMatrix matrix(A_block_size * cellsCount);

	initRightPart(lastLayerSolution, b_data);

	initMatrix(matrix);

	matrix.printToFile("resMatrix.txt");

	int n_amgx = A_block_size * cellsCount;
	int nnz_amgx = (A_block_size * A_block_size + 108) * cellsCount;//A_small_block_size * A_small_block_size * A_block_size * cellsCount * 4;

    AMGX_matrix_upload_all(A_amgx, n_amgx, nnz_amgx, 1, 1, matrix.ia, matrix.ja, matrix.a, 0);
	AMGX_vector_upload(b_amgx, n_amgx, 1, b_data);
	AMGX_vector_set_zero(solution_amgx, n_amgx, 1);
	AMGX_solver_setup(solver, A_amgx);

	AMGX_pin_memory(currentLayerSolution, n_amgx * sizeof(float));

	//AMGX_write_system(A_amgx, b_amgx, solution_amgx, "static_system.mtx");

	writeVTK_AMGX((char*)TASK_NAME, step, lastLayerSolution);

	while (t < TMAX) {
		t += TAU;
		step++;

		//printf("time step: %d\n", step);

		// ************ begin AMGX solver ************

		AMGX_solver_solve_with_0_initial_guess(solver, b_amgx, solution_amgx);

		//AMGX_write_system(A_amgx, b_amgx, solution_amgx, "system_1_step.mtx");

		AMGX_vector_download(solution_amgx, currentLayerSolution);

		copyToOld(lastLayerSolution, currentLayerSolution, n_amgx);

		initRightPart(lastLayerSolution, b_data);

		AMGX_vector_upload(b_amgx, n_amgx, 1, b_data);

		if (step % SAVE_STEP == 0)
		{
			writeVTK_AMGX((char*)TASK_NAME, step, currentLayerSolution);

			double L2 = 0.0;

			for (int i = 0; i < cellsCount; i++){
				L2 += cellS[i]*(currentLayerSolution[i * A_block_size + 6] - exact(t, cellCx[i], cellCy[i]))*(currentLayerSolution[i * A_block_size + 6] - exact(t, cellCx[i], cellCy[i]));
			}

			OutPut("L2 = %15.10f\n", sqrt(L2));
		}
		if (step % 100 == 0)
		{
			OutPut("%8d | time: %15.8e | U: %15.8e | WX: %15.8e | WY: %15.8e |\n", step, t, resU, resWx, resWy);
		}

		if(step == 700)
		{
			printf("runtime = %f", (clock() / 1000.0));
		}

		memset(currentLayerSolution, 0.0, sizeof(float) * A_block_size * cellsCount);
		AMGX_vector_set_zero(solution_amgx, n_amgx, 1);
	}

	AMGX_solver_destroy(solver);
	AMGX_vector_destroy(b_amgx);
	AMGX_vector_destroy(solution_amgx);
	AMGX_matrix_destroy(A_amgx);
	AMGX_resources_destroy(rsrc);

	AMGX_finalize_plugins();
	AMGX_finalize();
}

double exact(double t, double x, double y){
	return exp(-2.0 * M_PI * M_PI * t) * sin(M_PI * x) * sin(M_PI * y);
}

void copyToOld(float *lastLayerSolution, float *currentLayerSolution, int N) {
	for (int i = 0; i < N; i++) {
		lastLayerSolution[i] = currentLayerSolution[i];
	}
}

void GetK(double& kxx, double& kxy, double& kyx, double& kyy)
{
	kxx = 1.0;
	kyy = 1.0;
	kxy = 0.0;
	kyx = 0.0;
}

void initGrid()
{
	OutPut("Init grid...\n");
	nodesCount = (NX + 1)*(NY + 1);
	nodes = (Point*)malloc(nodesCount * sizeof(Point));
	int k = 0;
	for (int j = 0; j <= NY; j++)
	{
		double y = YMIN + HY*j;
		for (int i = 0; i <= NX; i++)
		{
			double x = XMIN + HX*i;
			nodes[k].x = x;
			nodes[k].y = y;
			k++;
		}
	}

	cellsCount = NX*NY * 2;
	cellS = (double*)malloc(cellsCount * sizeof(double));
	cellIsBound = (bool*)malloc(cellsCount * sizeof(bool));

	cellCx = (float*)malloc(cellsCount * sizeof(float));
	cellCy = (float*)malloc(cellsCount * sizeof(float));
	cellNodes = (int**)malloc(cellsCount * sizeof(int*));
	cellEdges = (int**)malloc(cellsCount * sizeof(int*));
	cellNeigh = (int**)malloc(cellsCount * sizeof(int*));
	for (int i = 0; i < cellsCount; i++)
	{
		cellNodes[i] = (int*)malloc(3 * sizeof(int));
		cellEdges[i] = (int*)malloc(3 * sizeof(int));
		cellNeigh[i] = (int*)malloc(3 * sizeof(int));
		cellNeigh[i][0] = -1;
		cellNeigh[i][1] = -1;
		cellNeigh[i][2] = -1;
	}

	edgesCount = NX*NY * 3 + NX + NY;
	edgeNode1 = (int*)malloc(edgesCount * sizeof(int));
	edgeNode2 = (int*)malloc(edgesCount * sizeof(int));
	edgeCell1 = (int*)malloc(edgesCount * sizeof(int));
	edgeCell2 = (int*)malloc(edgesCount * sizeof(int));
	edgeNormal = (Vector*)malloc(edgesCount * sizeof(Vector));
	edgeType = (int*)malloc(edgesCount * sizeof(int));
	edgeC = (Point*)malloc(edgesCount * sizeof(Point));
	edgeL = (double*)malloc(edgesCount * sizeof(double));

	int iCell = 0;
	int iEdge = 0;
	for (int j = 0; j < NY; j++)
	{
		for (int i = 0; i < NX; i++)
		{
			cellNodes[iCell][0] = i + (NX + 1)*j;
			cellNodes[iCell][1] = i + 1 + (NX + 1)*j;
			cellNodes[iCell][2] = i + (NX + 1)*(j + 1);

			cellCx[iCell] = (nodes[cellNodes[iCell][0]].x + nodes[cellNodes[iCell][1]].x + nodes[cellNodes[iCell][2]].x) / 3.0;
			cellCy[iCell] = (nodes[cellNodes[iCell][0]].y + nodes[cellNodes[iCell][1]].y + nodes[cellNodes[iCell][2]].y) / 3.0;

			edgeNode1[iEdge] = cellNodes[iCell][0];
			edgeNode2[iEdge] = cellNodes[iCell][1];
			edgeC[iEdge].x = (nodes[edgeNode1[iEdge]].x + nodes[edgeNode2[iEdge]].x) / 2.0;
			edgeC[iEdge].y = (nodes[edgeNode1[iEdge]].y + nodes[edgeNode2[iEdge]].y) / 2.0;
			edgeCell1[iEdge] = iCell;
			edgeCell2[iEdge] = iCell - NX * 2 + 1;
			edgeNormal[iEdge].x = nodes[edgeNode2[iEdge]].y - nodes[edgeNode1[iEdge]].y;
			edgeNormal[iEdge].y = nodes[edgeNode1[iEdge]].x - nodes[edgeNode2[iEdge]].x;
			edgeL[iEdge] = sqrt(edgeNormal[iEdge].x*edgeNormal[iEdge].x + edgeNormal[iEdge].y*edgeNormal[iEdge].y);
			edgeNormal[iEdge].x /= edgeL[iEdge];
			edgeNormal[iEdge].y /= edgeL[iEdge];
			cellAddNeigh(edgeCell1[iEdge], edgeCell2[iEdge]);
			if (j == 0)
			{
				edgeType[iEdge] = 1;
				cellIsBound[iCell] = true;
			}
			else {
				edgeType[iEdge] = 0;
				cellIsBound[iCell] = false;
			}
			iEdge++;

			edgeNode1[iEdge] = cellNodes[iCell][1];
			edgeNode2[iEdge] = cellNodes[iCell][2];
			edgeC[iEdge].x = (nodes[edgeNode1[iEdge]].x + nodes[edgeNode2[iEdge]].x) / 2.0;
			edgeC[iEdge].y = (nodes[edgeNode1[iEdge]].y + nodes[edgeNode2[iEdge]].y) / 2.0;
			edgeCell1[iEdge] = iCell;
			edgeCell2[iEdge] = iCell + 1;
			edgeNormal[iEdge].x = nodes[edgeNode2[iEdge]].y - nodes[edgeNode1[iEdge]].y;
			edgeNormal[iEdge].y = nodes[edgeNode1[iEdge]].x - nodes[edgeNode2[iEdge]].x;
			edgeL[iEdge] = sqrt(edgeNormal[iEdge].x*edgeNormal[iEdge].x + edgeNormal[iEdge].y*edgeNormal[iEdge].y);
			edgeNormal[iEdge].x /= edgeL[iEdge];
			edgeNormal[iEdge].y /= edgeL[iEdge];
			cellAddNeigh(edgeCell1[iEdge], edgeCell2[iEdge]);
			edgeType[iEdge] = 0;
			cellIsBound[iCell] = false;
			iEdge++;

			edgeNode1[iEdge] = cellNodes[iCell][2];
			edgeNode2[iEdge] = cellNodes[iCell][0];
			edgeC[iEdge].x = (nodes[edgeNode1[iEdge]].x + nodes[edgeNode2[iEdge]].x) / 2.0;
			edgeC[iEdge].y = (nodes[edgeNode1[iEdge]].y + nodes[edgeNode2[iEdge]].y) / 2.0;
			edgeCell1[iEdge] = iCell;
			edgeCell2[iEdge] = (i == 0) ? -1 : iCell - 1;
			edgeNormal[iEdge].x = nodes[edgeNode2[iEdge]].y - nodes[edgeNode1[iEdge]].y;
			edgeNormal[iEdge].y = nodes[edgeNode1[iEdge]].x - nodes[edgeNode2[iEdge]].x;
			edgeL[iEdge] = sqrt(edgeNormal[iEdge].x*edgeNormal[iEdge].x + edgeNormal[iEdge].y*edgeNormal[iEdge].y);
			edgeNormal[iEdge].x /= edgeL[iEdge];
			edgeNormal[iEdge].y /= edgeL[iEdge];
			cellAddNeigh(edgeCell1[iEdge], edgeCell2[iEdge]);
			if (i == 0)
			{
				edgeType[iEdge] = 4;
				cellIsBound[iCell] = true;
			}
			else {
				edgeType[iEdge] = 0;
				cellIsBound[iCell] = cellIsBound[iCell] || false;
			}
			iEdge++;

			iCell++;

			cellNodes[iCell][0] = i + 1 + (NX + 1)*j;
			cellNodes[iCell][1] = i + 1 + (NX + 1)*(j + 1);
			cellNodes[iCell][2] = i + (NX + 1)*(j + 1);

			cellCx[iCell] = (nodes[cellNodes[iCell][0]].x + nodes[cellNodes[iCell][1]].x + nodes[cellNodes[iCell][2]].x) / 3.0;
			cellCy[iCell] = (nodes[cellNodes[iCell][0]].y + nodes[cellNodes[iCell][1]].y + nodes[cellNodes[iCell][2]].y) / 3.0;

			iCell++;
		}

		iCell--;
		edgeNode1[iEdge] = cellNodes[iCell][0];
		edgeNode2[iEdge] = cellNodes[iCell][1];
		edgeC[iEdge].x = (nodes[edgeNode1[iEdge]].x + nodes[edgeNode2[iEdge]].x) / 2.0;
		edgeC[iEdge].y = (nodes[edgeNode1[iEdge]].y + nodes[edgeNode2[iEdge]].y) / 2.0;
		edgeCell1[iEdge] = iCell;
		edgeCell2[iEdge] = -1;
		edgeNormal[iEdge].x = nodes[edgeNode2[iEdge]].y - nodes[edgeNode1[iEdge]].y;
		edgeNormal[iEdge].y = nodes[edgeNode1[iEdge]].x - nodes[edgeNode2[iEdge]].x;
		edgeL[iEdge] = sqrt(edgeNormal[iEdge].x*edgeNormal[iEdge].x + edgeNormal[iEdge].y*edgeNormal[iEdge].y);
		edgeNormal[iEdge].x /= edgeL[iEdge];
		edgeNormal[iEdge].y /= edgeL[iEdge];
		cellAddNeigh(edgeCell1[iEdge], edgeCell2[iEdge]);
		edgeType[iEdge] = 2;
		cellIsBound[iCell] = true;
		iEdge++;

		iCell++;
	}

	for (int i = 0; i < NX; i++)
	{
		iCell--;
		edgeNode1[iEdge] = cellNodes[iCell][1];
		edgeNode2[iEdge] = cellNodes[iCell][2];
		edgeC[iEdge].x = (nodes[edgeNode1[iEdge]].x + nodes[edgeNode2[iEdge]].x) / 2.0;
		edgeC[iEdge].y = (nodes[edgeNode1[iEdge]].y + nodes[edgeNode2[iEdge]].y) / 2.0;
		edgeCell1[iEdge] = iCell;
		edgeCell2[iEdge] = -1;
		edgeNormal[iEdge].x = nodes[edgeNode2[iEdge]].y - nodes[edgeNode1[iEdge]].y;
		edgeNormal[iEdge].y = nodes[edgeNode1[iEdge]].x - nodes[edgeNode2[iEdge]].x;
		edgeL[iEdge] = sqrt(edgeNormal[iEdge].x*edgeNormal[iEdge].x + edgeNormal[iEdge].y*edgeNormal[iEdge].y);
		edgeNormal[iEdge].x /= edgeL[iEdge];
		edgeNormal[iEdge].y /= edgeL[iEdge];
		cellAddNeigh(edgeCell1[iEdge], edgeCell2[iEdge]);
		edgeType[iEdge] = 3;
		cellIsBound[iCell] = true;
		iEdge++;
		iCell--;
	}

    for (int i = 0; i < cellsCount; i++)
    {
        double a = edgeL[cellEdges[i][0]];
        double b = edgeL[cellEdges[i][1]];
        double c = edgeL[cellEdges[i][2]];
        double p = (a + b + c) / 2.0;
        cellS[i] = sqrt(p*(p - a)*(p - b)*(p - c));

        printf("%f\n", cellS[i]);
    }

	for (int i = 0; i < edgesCount; i++)
	{
		if (edgeCell2[i] < 0) edgeCell2[i] = -1;
	}

	cellDXx = (float*)malloc(cellsCount * sizeof(float));
	cellDXy = (float*)malloc(cellsCount * sizeof(float));
	for (int i = 0; i < cellsCount; i++)
	{
		cellDXx[i] = max(max(nodes[cellNodes[i][0]].x, nodes[cellNodes[i][1]].x), nodes[cellNodes[i][2]].x) - min(min(nodes[cellNodes[i][0]].x, nodes[cellNodes[i][1]].x), nodes[cellNodes[i][2]].x);
		cellDXy[i] = max(max(nodes[cellNodes[i][0]].y, nodes[cellNodes[i][1]].y), nodes[cellNodes[i][2]].y) - min(min(nodes[cellNodes[i][0]].y, nodes[cellNodes[i][1]].y), nodes[cellNodes[i][2]].y);
	}

	FILE * fp = fopen("grid.gnuplot.txt", "w");
	for (int i = 0; i < cellsCount; i++)
	{
		fprintf(fp, "%f %f \n", nodes[cellNodes[i][0]].x, nodes[cellNodes[i][0]].y);
		fprintf(fp, "%f %f \n", nodes[cellNodes[i][1]].x, nodes[cellNodes[i][1]].y);
		fprintf(fp, "%f %f \n", nodes[cellNodes[i][2]].x, nodes[cellNodes[i][2]].y);
		fprintf(fp, "%f %f \n", nodes[cellNodes[i][0]].x, nodes[cellNodes[i][0]].y);
		fprintf(fp, "\n");
	}
}

void cellAddNeigh(int c1, int c2)
{
	if (c1 < 0 || c2 < 0) return;

	int * neigh = cellNeigh[c1];
	if ((neigh[0] != c2) && (neigh[1] != c2) && (neigh[2] != c2))
	{
		int i = 0;
		while (neigh[i] >= 0) i++;
		neigh[i] = c2;
	}

	neigh = cellNeigh[c2];
	if ((neigh[0] != c1) && (neigh[1] != c1) && (neigh[2] != c1))
	{
		int i = 0;
		while (neigh[i] >= 0) i++;
		neigh[i] = c1;
	}
}

int __getEdgeByCells(int c1, int c2)
{
	for (int iEdge = 0; iEdge < edgesCount; iEdge++)
	{
		if ((edgeCell1[iEdge] == c1 && edgeCell2[iEdge] == c2) || (edgeCell1[iEdge] == c2 && edgeCell2[iEdge] == c1)) return iEdge;
	}
}

int __findEdge(int n1, int n2)
{
	for (int iEdge = 0; iEdge < edgesCount; iEdge++)
	{
		if ((edgeNode1[iEdge] == n1 && edgeNode2[iEdge] == n2) || (edgeNode1[iEdge] == n2 && edgeNode2[iEdge] == n1))
		{
			return iEdge;
		}
	}
	return -1;
}

void loadGrid(char* fName)
{
	char str[50];
	FILE *fp;
	int tmp;
	float tmp1;

	sprintf(str, "%s.node", fName);
	fp = fopen(str, "r");
	fscanf(fp, "%d %f %f %f", &nodesCount, &tmp1, &tmp1, &tmp1);
	nodes = new Point[nodesCount];
	for (int i = 0; i < nodesCount; i++)
	{
		fscanf(fp, "%d %lf %lf %d", &tmp, &(nodes[i].x), &(nodes[i].y), &tmp);
	}
	fclose(fp);

	sprintf(str, "%s.ele", fName);
	fp = fopen(str, "r");
	fscanf(fp, "%d %d %d", &cellsCount, &tmp, &tmp);
	cellNodes = new int*[cellsCount];
	cellNeigh = new int*[cellsCount];
	cellEdges = new int*[cellsCount];
	cellS = new double[cellsCount];

	cellCx = new float[cellsCount];
	cellCy = new float[cellsCount];

	cellDXx = new float[cellsCount];
	cellDXy = new float[cellsCount];
	cellType = new int[cellsCount];
	for (int i = 0; i < cellsCount; i++)
	{
		cellNodes[i] = new int[3];
		cellNeigh[i] = new int[3];
		cellEdges[i] = new int[3];
	}
	for (int i = 0; i < cellsCount; i++)
	{
		fscanf(fp, "%d %d %d %d %d", &tmp, &cellNodes[i][0], &cellNodes[i][1], &cellNodes[i][2], &cellType[i]);
		cellNodes[i][0]--;
		cellNodes[i][1]--;
		cellNodes[i][2]--;

		cellCx[i] = (nodes[cellNodes[i][0]].x + nodes[cellNodes[i][1]].x + nodes[cellNodes[i][2]].x) / 3.0;
		cellCy[i] = (nodes[cellNodes[i][0]].y + nodes[cellNodes[i][1]].y + nodes[cellNodes[i][2]].y) / 3.0;

		cellDXx[i] = _max_(fabs(nodes[cellNodes[i][0]].x - nodes[cellNodes[i][1]].x), fabs(nodes[cellNodes[i][1]].x - nodes[cellNodes[i][2]].x), fabs(nodes[cellNodes[i][0]].x - nodes[cellNodes[i][2]].x));
		cellDXy[i] = _max_(fabs(nodes[cellNodes[i][0]].y - nodes[cellNodes[i][1]].y), fabs(nodes[cellNodes[i][1]].y - nodes[cellNodes[i][2]].y), fabs(nodes[cellNodes[i][0]].y - nodes[cellNodes[i][2]].y));
	}
	fclose(fp);

	sprintf(str, "%s.neigh", fName);
	fp = fopen(str, "r");
	fscanf(fp, "%d %d", &tmp, &tmp);

	for (int i = 0; i < cellsCount; i++)
	{

		fscanf(fp, "%d %d %d %d", &tmp, &(cellNeigh[i][0]), &(cellNeigh[i][1]), &(cellNeigh[i][2]));
		cellNeigh[i][0]--;
		cellNeigh[i][1]--;
		cellNeigh[i][2]--;
	}

	fclose(fp);
	edgesCount = 0;
	for (int i = 0; i < cellsCount; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			int p = cellNeigh[i][j];
			if (p > -1)
			{
				for (int k = 0; k < 3; k++)
				{
					if (cellNeigh[p][k] == i) cellNeigh[p][k] = -1;
				}
				edgesCount++;
			}
			if (p == -2) edgesCount++;
		}
	}

	edgeNode1 = new int[edgesCount];
	edgeNode2 = new int[edgesCount];
	edgeNormal = new Vector[edgesCount];
	edgeL = new double[edgesCount];
	edgeCell1 = new int[edgesCount];
	edgeCell2 = new int[edgesCount];
	edgeType = new int[edgesCount];
	edgeC = new Point[edgesCount];

	int iEdge = 0;
	int * cfi = new int[cellsCount];
	for (int i = 0; i < cellsCount; i++)
	{
		cfi[i] = 0;
	}

	for (int i = 0; i < cellsCount; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			int p = cellNeigh[i][j];
			if (p != -1)
			{
				edgeNode1[iEdge] = cellNodes[i][(j + 1) % 3];
				edgeNode2[iEdge] = cellNodes[i][(j + 2) % 3];
				edgeC[iEdge].x = (nodes[edgeNode1[iEdge]].x + nodes[edgeNode2[iEdge]].x) / 2.0;
				edgeC[iEdge].y = (nodes[edgeNode1[iEdge]].y + nodes[edgeNode2[iEdge]].y) / 2.0;

				edgeNormal[iEdge].x = nodes[edgeNode2[iEdge]].y - nodes[edgeNode1[iEdge]].y;    //why???
				edgeNormal[iEdge].y = nodes[edgeNode1[iEdge]].x - nodes[edgeNode2[iEdge]].x;

				edgeL[iEdge] = sqrt(edgeNormal[iEdge].x*edgeNormal[iEdge].x + edgeNormal[iEdge].y*edgeNormal[iEdge].y);
				edgeNormal[iEdge].x /= edgeL[iEdge];
				edgeNormal[iEdge].y /= edgeL[iEdge];
				edgeCell1[iEdge] = i;
				cellEdges[i][cfi[i]] = iEdge;
				cfi[i]++;

				if (p > -1)
				{

					edgeCell2[iEdge] = p;
					cellEdges[p][cfi[p]] = iEdge;
					cfi[p]++;
					edgeType[iEdge] = 0;
				}
				if (p == -2)
				{
					edgeCell2[iEdge] = -2;
					edgeType[iEdge] = -1;
				}
				iEdge++;
			}
		}
	}

	sprintf(str, "%s.poly", fName);
	fp = fopen(str, "r");
	int bndCount;
	fscanf(fp, "%d %d %d %d", &tmp, &tmp, &tmp, &tmp);
	fscanf(fp, "%d %d", &bndCount, &tmp);
	for (int i = 0; i < bndCount; i++)
	{
		int n, n1, n2, type;
		fscanf(fp, "%d %d %d %d", &n, &n1, &n2, &type);
		n1--;
		n2--;
		int iEdge = __findEdge(n1, n2);
		if (iEdge >= 0) edgeType[iEdge] = type;
	}
	fclose(fp);

	for (int i = 0; i < cellsCount; i++)
	{
		double a = edgeL[cellEdges[i][0]];
		double b = edgeL[cellEdges[i][1]];
		double c = edgeL[cellEdges[i][2]];
		double p = (a + b + c) / 2.0;
		cellS[i] = sqrt(p*(p - a)*(p - b)*(p - c));

		printf("%f\n", cellS[i]);
	}

	delete[] cfi;
}

void initData()
{
	OutPut("Init data:\n");
	OutPut("  - allocate memory...\n");

	funcCount = 3;
	edgeGPCount = 2;
	cellGPCount = 3;

	int matrixDim = cellsCount * funcCount;

	u = (double*)malloc(cellsCount * sizeof(double));
	wx = (double*)malloc(cellsCount * sizeof(double));
	wy = (double*)malloc(cellsCount * sizeof(double));

	uOld = (double*)malloc(cellsCount * sizeof(double));
	wxOld = (double*)malloc(cellsCount * sizeof(double));
	wyOld = (double*)malloc(cellsCount * sizeof(double));

	fU = (float*)malloc(cellsCount * funcCount * sizeof(float));
	fWx = (float*)malloc(cellsCount * funcCount * sizeof(float));
	fWy = (float*)malloc(cellsCount * funcCount * sizeof(float));

	fUOld = (float*)malloc(cellsCount * funcCount * sizeof(float));
	fWxOld = (float*)malloc(cellsCount * funcCount * sizeof(float));
	fWyOld = (float*)malloc(cellsCount * funcCount * sizeof(float));

	intU = (double*)malloc(cellsCount * funcCount * sizeof(double));
	intWx = (double*)malloc(cellsCount * funcCount * sizeof(double));
	intWy = (double*)malloc(cellsCount * funcCount * sizeof(double));

	intFi = (double**)malloc(cellsCount * sizeof(double*));

	matrA = new float[matrixDim * matrixDim];
	matrInvA = new float[matrixDim * matrixDim];

	memset(matrA, 0, sizeof(float) * matrixDim * matrixDim);
	memset(matrInvA, 0, sizeof(float) * matrixDim * matrixDim);

	cellA = new float[funcCount * funcCount];
	cellInvA = new float[funcCount * funcCount];

	cellGPx = (float*)malloc(cellsCount * cellGPCount * sizeof(float));
	cellGPy = (float*)malloc(cellsCount * cellGPCount * sizeof(float));
	edgeGP = (Point**)malloc(edgesCount * sizeof(Point*));

	cellWGP = (float*)malloc(cellGPCount * sizeof(float));
	edgeWGP = (float*)malloc(edgeGPCount * sizeof(float));

	for (int i = 0; i < cellsCount; i++)
	{
		intFi[i] = (double*)malloc(funcCount * sizeof(double));
	}
	for (int i = 0; i < edgesCount; i++)
	{
		edgeGP[i] = (Point*)malloc(edgeGPCount * sizeof(Point));
	}
	cellJ = (float*)malloc(cellsCount * sizeof(float));

	edgeJ = (float*)malloc(edgesCount * sizeof(float));

	OutPut("  - init start data...\n");
	double x2 = (XMAX + XMIN) / 2.0;

	bool isBoundary = false;

	for (int i = 0; i < cellsCount; i++)
	{
		fU[i * funcCount + 0] = sin(cellCx[i] * M_PI) * sin(cellCy[i] * M_PI);
		fU[i * funcCount + 1] = 0.0;
		fU[i * funcCount + 2] = 0.0;

		fWx[i * funcCount + 0] = 0.0;
		fWy[i * funcCount + 0] = 0.0;
		fWx[i * funcCount + 1] = 0.0;
		fWy[i * funcCount + 1] = 0.0;
		fWx[i * funcCount + 2] = 0.0;
		fWy[i * funcCount + 2] = 0.0;
	}

	// ************************************************************************************************
	OutPut("  - init gaussian points...\n");
	for (int i = 0; i < cellsCount; i++)
	{
		calcCellGP(i);
		calcMatr(i);
	}

	for (int i = 0; i < edgesCount; i++)
	{
		calcEdgeGP(i);
	}

	copyToOld();
}

double baseF(int fNum, int iCell, double x, double y)
{
	switch (fNum)
	{
	case 0:
		return 1.0;
		break;
	case 1:
		return (x - cellCx[iCell]) / cellDXx[iCell];
		break;
	case 2:
		return (y - cellCy[iCell]) / cellDXy[iCell];
		break;
	case 3:
		return (x - cellCx[iCell])*(x - cellCx[iCell]) / cellDXx[iCell] / cellDXx[iCell];
		break;
	case 4:
		return (y - cellCy[iCell])*(y - cellCy[iCell]) / cellDXy[iCell] / cellDXy[iCell];
		break;
	case 5:
		return (x - cellCx[iCell])*(y - cellCy[iCell]) / cellDXx[iCell] / cellDXy[iCell];
		break;
	default:
		OutPut("ERROR! Wrong number of basic function.\n");
		exit(-1);
	}
}

double baseDFDx(int fNum, int iCell, double x, double y)
{
	switch (fNum)
	{
	case 0:
		return 0.0;
		break;
	case 1:
		return 1.0 / cellDXx[iCell];
		break;
	case 2:
		return 0.0;
		break;
	case 3:
		return ((x - cellCx[iCell]) + (x - cellCx[iCell])) / cellDXx[iCell] / cellDXx[iCell];
		break;
	case 4:
		return 0.0;
		break;
	case 5:
		return (y - cellCy[iCell]) / cellDXx[iCell] / cellDXy[iCell];
		break;
	default:
		OutPut("ERROR! Wrong number of basic function.\n");
		exit(-1);
	}
}

double baseDFDy(int fNum, int iCell, double x, double y)
{
	switch (fNum)
	{
	case 0:
		return 0.0;
		break;
	case 1:
		return 0.0;
		break;
	case 2:
		return 1.0 / cellDXy[iCell];
		break;
	case 3:
		return 0.0;
		break;
	case 4:
		return ((y - cellCy[iCell]) + (y - cellCy[iCell])) / cellDXy[iCell] / cellDXy[iCell];
		break;
	case 5:
		return (x - cellCx[iCell]) / cellDXx[iCell] / cellDXy[iCell];
		break;
	default:
		OutPut("ERROR! Wrong number of basic function.\n");
		exit(-1);
	}
}

void calcCellGP(int iCell)	//Ñäåëàë äëÿ 3¸õ òî÷åê.
{
	float a = 1.0 / 6.0;
	float b = 2.0 / 3.0;
	float x1 = nodes[cellNodes[iCell][0]].x;
	float y1 = nodes[cellNodes[iCell][0]].y;
	float x2 = nodes[cellNodes[iCell][1]].x;
	float y2 = nodes[cellNodes[iCell][1]].y;
	float x3 = nodes[cellNodes[iCell][2]].x;
	float y3 = nodes[cellNodes[iCell][2]].y;
	float a1 = x1 - x3;
	float a2 = y1 - y3;
	float b1 = x2 - x3;
	float b2 = y2 - y3;
	float c1 = x3;
	float c2 = y3;

	cellWGP[0] = 1.0 / 6.0;

	cellGPx[iCell * cellGPCount + 0] = a1*a + b1*a + c1;
	cellGPy[iCell * cellGPCount + 0] = a2*a + b2*a + c2;

	cellWGP[1] = 1.0 / 6.0;

	cellGPx[iCell * cellGPCount + 1] = a1*a + b1*b + c1;
	cellGPy[iCell * cellGPCount + 1] = a2*a + b2*b + c2;

	cellWGP[2] = 1.0 / 6.0;

	cellGPx[iCell * cellGPCount + 2] = a1*b + b1*a + c1;
	cellGPy[iCell * cellGPCount + 2] = a2*b + b2*a + c2;

	cellJ[iCell] = a1*b2 - a2*b1;
}

void calcEdgeGP(int iEdge)
{
	double gp1 = -1.0 / sqrt(3.0);
	double gp2 = 1.0 / sqrt(3.0);
	double x1 = nodes[edgeNode1[iEdge]].x;
	double y1 = nodes[edgeNode1[iEdge]].y;
	double x2 = nodes[edgeNode2[iEdge]].x;
	double y2 = nodes[edgeNode2[iEdge]].y;

	edgeWGP[0] = 1.0;
	edgeGP[iEdge][0].x = (x1 + x2) / 2.0 + gp1*(x2 - x1) / 2.0;
	edgeGP[iEdge][0].y = (y1 + y2) / 2.0 + gp1*(y2 - y1) / 2.0;

	edgeWGP[1] = 1.0;
	edgeGP[iEdge][1].x = (x1 + x2) / 2.0 + gp2*(x2 - x1) / 2.0;
	edgeGP[iEdge][1].y = (y1 + y2) / 2.0 + gp2*(y2 - y1) / 2.0;

	edgeJ[iEdge] = sqrt(pow_2(x2 - x1) + pow_2(y2 - y1))*0.5;
}

void calcGradientsVolIntegral()
{
	double intWxtmp, intWytmp;
	double gpU, gpWx, gpWy;

	for (int iCell = 0; iCell < cellsCount; iCell++)
	{
		for (int iF = 0; iF < funcCount; iF++)
		{
			intWxtmp = 0.0;
			intWytmp = 0.0;

			for (int iGP = 0; iGP < cellGPCount; iGP++)
			{
				getFields(gpU, gpWx, gpWy, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP], OLD);

				double bfDFDx = baseDFDx(iF, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
				double bfDFDy = baseDFDy(iF, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

				intWxtmp += cellWGP[iGP] * (gpU * bfDFDx);
				intWytmp += cellWGP[iGP] * (gpU * bfDFDy);
			}

			if (abs(intWxtmp) <= EPS) intWxtmp = 0.0;
			if (abs(intWytmp) <= EPS) intWytmp = 0.0;


			intWx[iCell * funcCount + iF] -= intWxtmp * cellJ[iCell];

			intWy[iCell * funcCount + iF] -= intWytmp * cellJ[iCell];
		}
	}
}

void calcIntegralVol()
{
	double intUtmp;
	double gpU, gpWx, gpWy;

	for (int iCell = 0; iCell < cellsCount; iCell++)
	{
		for (int iF = 0; iF < funcCount; iF++) {
			intUtmp = 0.0;

			for (int iGP = 0; iGP < cellGPCount; iGP++)
			{
				getFields(gpU, gpWx, gpWy, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP], NEW);

				double bfDFDx = baseDFDx(iF, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
				double bfDFDy = baseDFDy(iF, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

				intUtmp += cellWGP[iGP] * (gpWx * bfDFDx + gpWy * bfDFDy);
			}

			if (abs(intUtmp) <= EPS) intUtmp = 0.0;

			intU[iCell * funcCount + iF] -= intUtmp * cellJ[iCell];
		}
	}
}

void calcGradientsSurfIntegral()
{
	double FU, FWx, FWy;
	double Ul = 0.0, Wxl = 0.0, Wyl = 0.0;
	double Ur = 0.0, Wxr = 0.0, Wyr = 0.0;

	double kxx, kyy, kxy, kyx;
	GetK(kxx, kxy, kyx, kyy);

	int c1, c2;
	bool isBoundary = false;
	for (int i = 0; i < edgesCount; i++)
	{
		c1 = edgeCell1[i];
		c2 = edgeCell2[i];
		if (c2 >= 0) {
			isBoundary = false;

			for (int iF = 0; iF < funcCount; iF++)
			{
				double tmpIntWx1 = 0.0, tmpIntWy1 = 0.0;
				double tmpIntWx2 = 0.0, tmpIntWy2 = 0.0;
				for (int iGP = 0; iGP < edgeGPCount; iGP++)
				{
					getFields(Ul, Wxl, Wyl, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y, OLD);
					getFields(Ur, Wxr, Wxr, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y, OLD);
					getFlux(FU, FWx, FWy, Ul, Wxl, Wyl, Ur, Wxr, Wyr, edgeNormal[i], isBoundary);

					double cGP1 = edgeWGP[iGP] * baseF(iF, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y);
					double cGP2 = edgeWGP[iGP] * baseF(iF, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y);

					tmpIntWx1 += (kxx * FU * cGP1 * edgeNormal[i].x);
					tmpIntWy1 += (kyy * FU * cGP1 * edgeNormal[i].y);

					tmpIntWx2 += (kxx * FU * cGP2 * edgeNormal[i].x);
					tmpIntWy2 += (kyy * FU * cGP2 * edgeNormal[i].y);
				}

				if (abs(tmpIntWx1) <= EPS) tmpIntWx1 = 0.0;
				if (abs(tmpIntWy1) <= EPS) tmpIntWy1 = 0.0;

				intWx[c1 * funcCount + iF] += tmpIntWx1 * edgeJ[i];

				intWy[c1 * funcCount + iF] += tmpIntWy1 * edgeJ[i];

				if (abs(tmpIntWx2) <= EPS) tmpIntWx2 = 0.0;
				if (abs(tmpIntWy2) <= EPS) tmpIntWy2 = 0.0;

				intWx[c2 * funcCount + iF] -= tmpIntWx2 * edgeJ[i];

				intWy[c2 * funcCount + iF] -= tmpIntWy2 * edgeJ[i];
			}
		}
		else {
			isBoundary = true;

			for (int iF = 0; iF < funcCount; iF++)
			{
				double tmpIntWx1 = 0.0, tmpIntWy1 = 0.0;
				for (int iGP = 0; iGP < edgeGPCount; iGP++)
				{
					getFields(Ul, Wxl, Wyl, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y, OLD);

					double tmp = 0;
					getFlux(FU, FWx, FWy, Ul, Wxl, Wyl, tmp, tmp, tmp, edgeNormal[i], isBoundary);

					double cGP1 = edgeWGP[iGP] * baseF(iF, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y);

					tmpIntWx1 += (kxx * FU * cGP1 * edgeNormal[i].x);
					tmpIntWy1 += (kyy * FU * cGP1 * edgeNormal[i].y);
				}

				if (abs(tmpIntWx1) <= EPS) tmpIntWx1 = 0.0;
				if (abs(tmpIntWy1) <= EPS) tmpIntWy1 = 0.0;

				intWx[c1 * funcCount + iF] += tmpIntWx1 * edgeJ[i];

				intWy[c1 * funcCount + iF] += tmpIntWy1 * edgeJ[i];
			}
		}
	}
}

void calcIntegralSurf()
{
	double FU, FWx, FWy;

	double Ul = 0.0, Wxl = 0.0, Wyl = 0.0;
	double Ur = 0.0, Wxr = 0.0, Wyr = 0.0;


	int c1, c2;
	bool isBoundary = false;
	for (int i = 0; i < edgesCount; i++)
	{
		c1 = edgeCell1[i];
		c2 = edgeCell2[i];
		if (c2 >= 0) {
			isBoundary = false;

			for (int iF = 0; iF < funcCount; iF++)
			{
				double tmpIntU1 = 0.0;
				double tmpIntU2 = 0.0;

				for (int iGP = 0; iGP < edgeGPCount; iGP++)
				{
					getFields(Ul, Wxl, Wyl, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y, NEW);
					getFields(Ur, Wxr, Wxr, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y, NEW);
					getFlux(FU, FWx, FWy, Ul, Wxl, Wyl, Ur, Wxr, Wyr, edgeNormal[i], isBoundary);

					double cGP1 = edgeWGP[iGP] * baseF(iF, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y);
					double cGP2 = edgeWGP[iGP] * baseF(iF, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y);

					tmpIntU1 += (edgeNormal[i].x * FWx * cGP1) + (edgeNormal[i].y * FWy * cGP1);

					tmpIntU2 += (edgeNormal[i].x * FWx * cGP2) + (edgeNormal[i].y * FWy * cGP2);
				}

				if (abs(tmpIntU1) <= EPS) tmpIntU1 = 0.0;

				intU[c1 * funcCount + iF] += tmpIntU1 * edgeJ[i];

				if (abs(tmpIntU2) <= EPS) tmpIntU2 = 0.0;

				intU[c2 * funcCount + iF] -= tmpIntU2 * edgeJ[i];
			}
		}
		else {
			isBoundary = true;

			for (int iF = 0; iF < funcCount; iF++)
			{
				double tmpIntU1 = 0.0;
				for (int iGP = 0; iGP < edgeGPCount; iGP++)
				{
					getFields(Ul, Wxl, Wyl, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y, NEW);

					double tmp = 0;
					getFlux(FU, FWx, FWy, Ul, Wxl, Wyl, tmp, tmp, tmp, edgeNormal[i], isBoundary);

					double cGP1 = edgeWGP[iGP] * baseF(iF, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y);

					tmpIntU1 += (edgeNormal[i].x * FWx * cGP1) + (edgeNormal[i].y * FWy * cGP1);
				}

				if (abs(tmpIntU1) <= EPS) tmpIntU1 = 0.0;

				intU[c1 * funcCount + iF] += tmpIntU1 * edgeJ[i];
			}
		}
	}
}

void getMatrixInCell(int icell, float *A)
{
	int matrixDim = cellsCount * funcCount;

	int matrixRowStep = icell * funcCount * matrixDim;
	int matrixColStep = icell * funcCount;

	int matrAIndex = 0;

	for (int i = 0; i < funcCount; i++)
	{
		for (int j = 0; j < funcCount; j++)
		{
			matrAIndex = matrixRowStep + i * matrixDim + matrixColStep + j;
			A[i * funcCount + j] = matrA[matrAIndex];
		}
	}
}

void getInvMatrixInCell(int icell, float *invA)
{
	int matrixDim = cellsCount * funcCount;

	int matrixRowStep = icell * funcCount * matrixDim;
	int matrixColStep = icell * funcCount;

	int matrAIndex = 0;

	for (int i = 0; i < funcCount; i++)
	{
		for (int j = 0; j < funcCount; j++)
		{
			matrAIndex = matrixRowStep + i * matrixDim + matrixColStep + j;
			invA[i * funcCount + j] = matrInvA[matrAIndex];
		}
	}
}

void setMatrixInCell(int icell, float *A)
{
	int matrixDim = cellsCount * funcCount;

	int matrixRowStep = icell * funcCount * matrixDim;
	int matrixColStep = icell * funcCount;

	int matrAIndex = 0;

	for (int i = 0; i < funcCount; i++)
	{
		for (int j = 0; j < funcCount; j++)
		{
			matrAIndex = matrixRowStep + i * matrixDim + matrixColStep + j;
			matrA[matrAIndex] = A[i * funcCount + j];
		}
	}
}

void setInvMatrixInCell(int icell, float *invA)
{
	int matrixDim = cellsCount * funcCount;

	int matrixRowStep = icell * funcCount * matrixDim;
	int matrixColStep = icell * funcCount;

	int matrAIndex = 0;

	for (int i = 0; i < funcCount; i++)
	{
		for (int j = 0; j < funcCount; j++)
		{
			matrAIndex = matrixRowStep + i * matrixDim + matrixColStep + j;
			matrInvA[matrAIndex] = invA[i * funcCount + j];
		}
	}
}

void calcMatr(int iCell)
{
	int& N = funcCount;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			cellA[i * N + j] = 0.0;
			for (int iGP = 0; iGP < cellGPCount; iGP++)
			{
				cellA[i * N + j] += cellWGP[iGP] * baseF(i, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP])*baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
			}

			cellA[i * N + j] *= cellJ[iCell];
		}
	}

	setMatrixInCell(iCell, cellA);

	inverseMatr(cellA, cellInvA, N);

	setInvMatrixInCell(iCell, cellInvA);
}

void inverseMatr__(double** a_src, double **am, int N)
{
	double **a = a_src;
	double detA = a[0][0] * a[1][1] * a[2][2] + a[1][0] * a[2][1] * a[0][2] + a[0][1] * a[1][2] * a[2][0]
		- a[2][0] * a[1][1] * a[0][2] - a[1][0] * a[0][1] * a[2][2] - a[0][0] * a[2][1] * a[1][2];

	double m[3][3];
	m[0][0] = a[1][1] * a[2][2] - a[2][1] * a[1][2];
	m[0][1] = a[2][0] * a[1][2] - a[1][0] * a[2][2];
	m[0][2] = a[1][0] * a[2][1] - a[2][0] * a[1][1];
	m[1][0] = a[2][1] * a[0][2] - a[0][1] * a[2][2];
	m[1][1] = a[0][0] * a[2][2] - a[2][0] * a[0][2];
	m[1][2] = a[2][0] * a[0][1] - a[0][0] * a[2][1];
	m[2][0] = a[0][1] * a[1][2] - a[1][1] * a[0][2];
	m[2][1] = a[1][0] * a[0][2] - a[0][0] * a[1][2];
	m[2][2] = a[0][0] * a[1][1] - a[1][0] * a[0][1];

	am[0][0] = m[0][0] / detA;
	am[0][1] = m[1][0] / detA;
	am[0][2] = m[2][0] / detA;
	am[1][0] = m[0][1] / detA;
	am[1][1] = m[1][1] / detA;
	am[1][2] = m[2][1] / detA;
	am[2][0] = m[0][2] / detA;
	am[2][1] = m[1][2] / detA;
	am[2][2] = m[2][2] / detA;
}

void inverseMatr(float* a_src, float *am, int N)
{
	int	*	mask;
	double	fmaxval;
	int		maxind;
	int		tmpi;
	double	tmp;

	double	**a;

	mask = new int[N];
	a = new double*[N];
	for (int i = 0; i < N; i++)
	{
		a[i] = new double[N];
		for (int j = 0; j < N; j++)
		{
			a[i][j] = a_src[i * N + j];
		}
	}

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			if (i == j)
			{
				am[i * N + j] = 1.0;
			}
			else {
				am[i * N + j] = 0.0;
			}
		}
	}
	for (int i = 0; i < N; i++)
	{
		mask[i] = i;
	}
	for (int i = 0; i < N; i++)
	{
		maxind = i;
		fmaxval = fabs(a[i][i]);
		for (int ni = i + 1; ni < N; ni++)
		{
			if (fabs(fmaxval) <= fabs(a[ni][i]))
			{
				fmaxval = fabs(a[ni][i]);
				maxind = ni;
			}
		}
		fmaxval = a[maxind][i];
		if (fmaxval == 0)
		{
			OutPut("ERROR! Determinant of mass matrix is zero...\n");
			return;
		}
		if (i != maxind)
		{
			for (int nj = 0; nj < N; nj++)
			{
				tmp = a[i][nj];
				a[i][nj] = a[maxind][nj];
				a[maxind][nj] = tmp;

				tmp = am[i * N + nj];
				am[i * N + nj] = am[maxind * N + nj];
				am[maxind * N + nj] = tmp;
			}
			tmpi = mask[i];
			mask[i] = mask[maxind];
			mask[maxind] = tmpi;
		}
		double aii = a[i][i];
		for (int j = 0; j < N; j++)
		{
			a[i][j] = a[i][j] / aii;
			am[i * N + j] = am[i * N + j] / aii;
		}
		for (int ni = 0; ni < N; ni++)
		{
			if (ni != i)
			{
				double fconst = a[ni][i];
				for (int nj = 0; nj < N; nj++)
				{
					a[ni][nj] = a[ni][nj] - fconst *  a[i][nj];
					am[ni * N + nj] = am[ni * N + nj] - fconst * am[i * N + nj];
				}
			}
		}
	}

	for (int i = 0; i < N; i++)
	{
		delete[] a[i];
	}
	delete[] a;
	delete[] mask;
	return;
}

void getFlux(double& FU, double& FWx, double& FWy,
	double Ul, double Wxl, double Wyl,
	double Ur, double Wxr, double Wyr, Vector n, bool isBoundary)
{
	double c11 = 1;

	if (!isBoundary)
	{
		FU = (Ul + Ur) * 0.5;
		FWx = (Wxl + Wxr) * 0.5 + c11 * (Ur - Ul) * n.x;
		FWy = (Wyl + Wyr) * 0.5 + c11 * (Ur - Ul) * n.y;
	}
	else
	{
		FU = 0.0;
		FWx = Wxl + c11 * (FU - Ul) * n.x;
		FWy = Wyl + c11 * (FU - Ul) * n.y;
	}
}

void getFields(double& U, double& Wx, double& Wy, int iCell, double x, double y, DATA_LAYER layer)
{
	double *fieldU, *fieldWx, *fieldWy;
	fieldU = (double*)malloc(funcCount * sizeof(double));
	fieldWx = (double*)malloc(funcCount * sizeof(double));
	fieldWy = (double*)malloc(funcCount * sizeof(double));

	switch (layer)
	{
	case OLD:
		for (int i = 0; i < funcCount; i++)
		{
			fieldU[i] = fUOld[iCell * funcCount + i];
			fieldWx[i] = fWxOld[iCell * funcCount + i];
			fieldWy[i] = fWyOld[iCell * funcCount + i];
		}
		break;
	case NEW:
	default:
		for (int i = 0; i < funcCount; i++)
		{
			fieldU[i] = fU[iCell * funcCount + i];
			fieldWx[i] = fWx[iCell * funcCount + i];
			fieldWy[i] = fWy[iCell * funcCount + i];
		}
		break;
	}

	U = fieldU[0];
	Wx = fieldWx[0];
	Wy = fieldWy[0];

	for (int j = 1; j < funcCount; j++)
	{
		double bF = baseF(j, iCell, x, y);

		U += fieldU[j] * bF;
		Wx += fieldWx[j] * bF;
		Wy += fieldWy[j] * bF;
	}

	free(fieldU);
	free(fieldWx);
	free(fieldWy);
}

void calcNewGradientValues()
{
	float *aWx, *aWy;
	aWx = (float*)malloc(sizeof(float)* funcCount);
	aWy = (float*)malloc(sizeof(float)* funcCount);

	for (int iCell = 0; iCell < cellsCount; iCell++)
	{
		getInvMatrixInCell(iCell, cellInvA);

		memset(aWx, 0, sizeof(float)*funcCount);
		memset(aWy, 0, sizeof(float)*funcCount);

		for (int j = 0; j < funcCount; j++)
		{
			for (int k = 0; k < funcCount; k++)
			{
				aWx[j] += cellInvA[j * funcCount + k] * intWx[iCell * funcCount + k];
				aWy[j] += cellInvA[j * funcCount + k] * intWy[iCell * funcCount + k];
			}
		}

		for (int j = 0; j < funcCount; j++)
		{
			fWx[iCell * funcCount + j] = aWx[j];
			fWy[iCell * funcCount + j] = aWy[j];

			if (abs(fWx[iCell * funcCount + j]) <= EPS) fWx[iCell * funcCount + j] = 0.0;
			if (abs(fWy[iCell * funcCount + j]) <= EPS) fWy[iCell * funcCount + j] = 0.0;
		}
	}

	free(aWx);
	free(aWy);
}

void calcNewValues()
{
	float *aU;
	aU = (float*)malloc(sizeof(float) * funcCount);

	for (int iCell = 0; iCell < cellsCount; iCell++)
	{

        getInvMatrixInCell(iCell, cellInvA);

        memset(aU, 0, sizeof(float) * funcCount);

        for (int j = 0; j < funcCount; j++)
        {
            for (int k = 0; k < funcCount; k++)
            {
                aU[j] += cellInvA[j * funcCount + k] * intU[iCell * funcCount + k];
            }
        }

        for (int j = 0; j < funcCount; j++)
        {
            fU[iCell * funcCount + j] += TAU * aU[j];

            if (abs(fU[iCell * funcCount + j]) <= EPS)
            {
                fU[iCell * funcCount + j] = 0.0;
            }
        }
	}

	free(aU);
}

void copyToOld()
{
	for (int i = 0; i < cellsCount; i++)
	{
		for (int j = 0; j < funcCount; j++)
		{
			fUOld[i * funcCount + j] = fU[i * funcCount + j];
			fWxOld[i * funcCount + j] = fWx[i * funcCount + j];
			fWyOld[i * funcCount + j] = fWy[i * funcCount + j];
		}
	}

	memcpy(uOld, u, cellsCount * sizeof(double));
	memcpy(wxOld, wx, cellsCount * sizeof(double));
	memcpy(wyOld, wy, cellsCount * sizeof(double));
}

double max(double a, double b)
{
	if (a > b) return a;
	return b;
}

double max(double a, double b, double c)
{
	return max(max(a, b), c);
}

double min(double a, double b, double c)
{
	return min(min(a, b), c);
}

double min(double a, double b)
{
	if (a < b) return a;
	return b;
}

void destroyData()
{
	free(u);
	free(wx);
	free(wy);
	free(uOld);
	free(wxOld);
	free(wyOld);

	for (int i = 0; i < edgesCount; i++)
	{
		free(edgeGP[i]);
	}

	free(matrA);
	free(matrInvA);

	free(cellA);
	free(cellInvA);

	free(intU);
	free(intWx);
	free(intWy);
	free(edgeGP);
	free(cellGPx);
	free(cellGPy);
	free(cellWGP);
	free(edgeWGP);
	free(fU);
	free(fWx);
	free(fWy);
	free(fWxOld);
	free(fWyOld);
	free(fUOld);

	free(cellJ);
	free(edgeJ);
}

void destroyGrid()
{
	for (int i = 0; i < cellsCount; i++)
	{
		free(cellNodes[i]);
		free(cellNeigh[i]);
		free(cellEdges[i]);
	}
	free(cellNodes);
	free(cellEdges);
	free(cellS);
	free(cellIsBound);
	free(cellCx);
	free(cellCy);

	free(cellType);
	free(edgeType);
	free(edgeNode1);
	free(edgeNode2);
	free(edgeCell1);
	free(edgeCell2);
	free(edgeNormal);
	free(edgeC);
	free(edgeL);
}

void writeVTK(char* name, int step)
{
	char fName[50];
	FILE * fp;
	sprintf(fName, "%s.%010d.vtk", name, step);
	fp = fopen(fName, "w");
	fprintf(fp, "");
	fprintf(fp, "# vtk DataFile Version 2.0\n");
	fprintf(fp, "2D RKDG method for task '%s' results.\n", name);
	fprintf(fp, "ASCII\n");
	fprintf(fp, "DATASET UNSTRUCTURED_GRID\n");

	fprintf(fp, "POINTS %d float\n", nodesCount);
	for (int i = 0; i < nodesCount; i++)
	{
		fprintf(fp, "%f %f %f  \n", nodes[i].x, nodes[i].y, 0.0);
	}
	fprintf(fp, "\n");

	fprintf(fp, "CELLS %d %d\n", cellsCount, 4 * cellsCount);
	for (int i = 0; i < cellsCount; i++)
	{
		fprintf(fp, "3 %d %d %d  \n", cellNodes[i][0], cellNodes[i][1], cellNodes[i][2]);
	}
	fprintf(fp, "\n");

	fprintf(fp, "CELL_TYPES %d\n", cellsCount);
	for (int i = 0; i < cellsCount; i++) fprintf(fp, "5\n");
	fprintf(fp, "\n");

	fprintf(fp, "CELL_DATA %d\n", cellsCount);

	fprintf(fp, "SCALARS Temperature float 1\nLOOKUP_TABLE default\n");
	for (int i = 0; i < cellsCount; i++)
	{
		fprintf(fp, "%f ", fU[i * funcCount + 0]);
		if ((i + 1) % 8 == 0 || i + 1 == cellsCount) fprintf(fp, "\n");
	}

	fclose(fp);
}

void writeVTK_AMGX(char* name, int step, float* currentLayerSolution) {
	char fName[50];
	FILE *fp;
	sprintf(fName, "%s.%010d.vtk", name, step);
	fp = fopen(fName, "w");
	//fprintf(fp, "");
	fprintf(fp, "# vtk DataFile Version 2.0\n");
	fprintf(fp, "2D RKDG method for task '%s' results.\n", name);
	fprintf(fp, "ASCII\n");
	fprintf(fp, "DATASET UNSTRUCTURED_GRID\n");

	fprintf(fp, "POINTS %d float\n", nodesCount);
	for (int i = 0; i < nodesCount; i++) {
		fprintf(fp, "%f %f %f  \n", nodes[i].x, nodes[i].y, 0.0);
	}

	fprintf(fp, "\n");

	fprintf(fp, "CELLS %d %d\n", cellsCount, 4 * cellsCount);
	for (int i = 0; i < cellsCount; i++) {
		fprintf(fp, "3 %d %d %d  \n", cellNodes[i][0], cellNodes[i][1], cellNodes[i][2]);
	}

	fprintf(fp, "\n");

	fprintf(fp, "CELL_TYPES %d\n", cellsCount);

	for (int i = 0; i < cellsCount; i++) fprintf(fp, "5\n");

	fprintf(fp, "\n");

	fprintf(fp, "CELL_DATA %d\n", cellsCount);

	fprintf(fp, "SCALARS Temperature float 1\nLOOKUP_TABLE default\n");
	for (int i = 0; i < cellsCount; i++) {
		fprintf(fp, "%f ", currentLayerSolution[i * A_block_size + 6]);
		if ((i + 1) % 8 == 0 || i + 1 == cellsCount) fprintf(fp, "\n");
	}

	fclose(fp);
}

void save(int step)
{
	char fName[50];
	FILE * fp;

	sprintf(fName, "res_%010d.gnuplot.txt", step);
	fp = fopen(fName, "w");
	for (int i = 0; i < 300; i++)
	{
		for (int j = 0; j < 100; j++)
		{
			double	x = 3.0*i / 300;
			double	y = 1.0*j / 100;
			if (x > 0.6 && y < 0.2)
			{
				fprintf(fp, "%16.8e %16.8e %16.8e %16.8e %16.8e %16.8e %16.8e\n", x, y, 0.0, 0.0, 0.0, 0.0, 0.0);
			}
			else
			{
				double	minL = 1.e+10;
				int		minI = 0;
				for (int iC = 0; iC < cellsCount; iC++)
				{
					float &ptx = cellCx[iC];
					float &pty = cellCy[iC];

					double L = sqrt(pow_2(x - ptx) + pow_2(y - pty));
					if (L < minL)
					{
						minL = L;
						minI = iC;
					}
				}
				double uCurrent, wxCurrent, wyCurrent;
				getFields(uCurrent, wxCurrent, wyCurrent, minI, x, y, NEW);

				fprintf(fp, "%16.8e %16.8e %16.8e %16.8e %16.8e\n", x, y, uCurrent, wxCurrent, wyCurrent);
			}
		}
	}

	fclose(fp);


	sprintf(fName, "res_%010d.cells.txt", step);
	fp = fopen(fName, "w");
	for (int j = 0; j < cellsCount; j++)
	{
		double uCurrent, wxCurrent, wyCurrent;
		getFields(uCurrent, wxCurrent, wyCurrent, j, cellCx[j], cellCy[j], NEW);

		fprintf(fp, "%d %16.8e %16.8e %16.8e %16.8e %16.8e\n", j, cellCx[j], cellCy[j], uCurrent, wxCurrent, wyCurrent);
	}
	fclose(fp);
}

void OutPut(const char* str, ...)
{
	va_list list;

	va_start(list, str);
	vprintf(str, list);
	::fflush(stdout);
	va_end(list);

	if (fileLog) {
		va_start(list, str);
		vfprintf(fileLog, str, list);
		va_end(list);
		::fflush(fileLog);
	} // if
} // OutPut

void initRightPart(float *lastLayerSolution, float *rightPart_data)
{
	const int uBlock = 2;

	int iCell = 0;
	int iSmallBlockId = 0;

	A_size = A_block_size * cellsCount;

	for (int i = 0; i < A_size; i++) {

		if (((i % A_small_block_size) == 0) && (i != 0)) {
			iSmallBlockId++;
		}

		if (((i % A_block_size) == 0) && (i != 0)) {
			iCell++;
			iSmallBlockId = 0;
		}

		// rowId in each small block
		int rowId = i % A_small_block_size;

		float fullInt = 0.0;

		if (iSmallBlockId == uBlock) {
			for (int j = 0; j < A_small_block_size; j++)
			{
				float tmpInt = 0.0;

				int iRowU = (iCell * A_block_size + 6) + j;

				for (int iGP = 0; iGP < cellGPCount; iGP++)
				{
					tmpInt += cellWGP[iGP] * baseF(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]) * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
				}

				tmpInt *= (cellJ[iCell] * lastLayerSolution[iRowU]);

				//tmpInt /= TAU;

				fullInt += tmpInt;
			}

			rightPart_data[i] = fullInt;
		}
	}
}

void initRightPart_hypre(MatrixSolver *hypreSolver)
{
    const int uBlock = 2;

    int iCell = 0;
    int iSmallBlockId = 0;

    A_size = A_block_size * cellsCount;

    for (int i = 0; i < A_size; i++) {

        if (((i % A_small_block_size) == 0) && (i != 0)) {
            iSmallBlockId++;
        }

        if (((i % A_block_size) == 0) && (i != 0)) {
            iCell++;
            iSmallBlockId = 0;
        }

        // rowId in each small block
        int rowId = i % A_small_block_size;

        float fullInt = 0.0;

        if (iSmallBlockId == uBlock) {
            for (int j = 0; j < A_small_block_size; j++)
            {
                float tmpInt = 0.0;

                int iRowU = (iCell * A_block_size + 6) + j;

                for (int iGP = 0; iGP < cellGPCount; iGP++)
                {
                    tmpInt += cellWGP[iGP] * baseF(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]) * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                }

                tmpInt *= (cellJ[iCell] * hypreSolver->x[iRowU]);

                //tmpInt /= TAU;

                fullInt += tmpInt;
            }

            hypreSolver->setRightElement(i, fullInt);
        }
    }
}

void initMatrix_hypre(MatrixSolver *hypreSolver)
{
    const int qxBlock = 0;
    const int qyBlock = 1;
    const int uBlock = 2;

    A_size = A_block_size * cellsCount;	// 9 * cellsCount

    int iCell = 0;
    int iSmallBlockId = 0;

    // begin matrix initialization
    for (int i = 0; i < A_size; i++)
    {
        if ((i % A_small_block_size) == 0 && (i != 0))
        {
            iSmallBlockId++;
        }

        if ((i % A_block_size) == 0 && (i != 0))
        {
            iCell++;
            iSmallBlockId = 0;
        }

        // variable for current column
        int colId = iCell * A_block_size + iSmallBlockId * A_small_block_size;

        // rowId in each small block
        int rowId = i % A_small_block_size;

        // сalculate the same values of the matrix A
        for (int j = 0; j < A_small_block_size; j++)
        {
            float tmpInt = 0.0;
            //A.set(i, colId + j, 0.0);

            for (int iGP = 0; iGP < cellGPCount; iGP++)
            {
                tmpInt += cellWGP[iGP] * baseF(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]) * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
            }

            tmpInt *= cellJ[iCell];

            if (iSmallBlockId == uBlock)
            {
                //tmpInt /= TAU;
            }

            hypreSolver->setMatrElement(i, colId + j, tmpInt);
        }

        // calculate vol integral values for qx-block
        if (iSmallBlockId == qxBlock)
        {
            int uColId = colId + uBlock * A_small_block_size;

            for (int j = 0; j < A_small_block_size; j++)
            {
                float tmpInt = 0.0;

                for (int iGP = 0; iGP < cellGPCount; iGP++)
                {
                    float bfDFDx = baseDFDx(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

                    tmpInt += cellWGP[iGP] * bfDFDx * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                }

                if (abs(tmpInt) <= EPS) tmpInt = 0.0;

                tmpInt *= cellJ[iCell];

                hypreSolver->setMatrElement(i, uColId + j, tmpInt);
            }
        }
            // calculate vol integral values for qy-block
        else if (iSmallBlockId == qyBlock)
        {
            int uColId = colId + A_small_block_size;

            for (int j = 0; j < A_small_block_size; j++)
            {
                float tmpInt = 0.0;

                for (int iGP = 0; iGP < cellGPCount; iGP++)
                {
                    float bfDFDy = baseDFDy(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

                    tmpInt += cellWGP[iGP] * bfDFDy * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                }

                if (abs(tmpInt) <= EPS) tmpInt = 0.0;

                tmpInt *= cellJ[iCell];

                hypreSolver->setMatrElement(i, uColId + j, tmpInt);
            }
        }
            // calculate vol integral values for u-block
        else if (iSmallBlockId == uBlock)
        {
            int qxColId = colId - uBlock * A_small_block_size;
            int qyColId = colId - A_small_block_size;

            for (int j = 0; j < A_small_block_size; j++)
            {
                float tmpInt1 = 0.0;
                float tmpInt2 = 0.0;

                for (int iGP = 0; iGP < cellGPCount; iGP++)
                {
                    float bfDFDx = baseDFDx(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                    float bfDFDy = baseDFDy(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

                    tmpInt1 += cellWGP[iGP] * bfDFDx * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                    tmpInt2 += cellWGP[iGP] * bfDFDy * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                }

                if (abs(tmpInt1) <= EPS) tmpInt1 = 0.0;
                if (abs(tmpInt2) <= EPS) tmpInt2 = 0.0;

                tmpInt1 *= cellJ[iCell] * TAU;
                tmpInt2 *= cellJ[iCell] * TAU;

                hypreSolver->setMatrElement(i, qxColId + j, tmpInt1);
                hypreSolver->setMatrElement(i, qyColId + j, tmpInt2);
            }
        }
    }

    // ########################################################################################################################################
    // ################################################## Chinese flux ########################################################################
    // ########################################################################################################################################

    int c1, c2;
    for (int i = 0; i < edgesCount; i++)
    {
        c1 = edgeCell1[i];
        c2 = edgeCell2[i];

        int iRowU1 = c1 * A_block_size + 6;
        int iRowU2 = c2 * A_block_size + 6;
        int iRowQy1 = c1 * A_block_size + 3;
        int iRowQy2 = c2 * A_block_size + 3;
        int iRowQx1 = c1 * A_block_size;
        int iRowQx2 = c2 * A_block_size;

        int iColU1_qx = c1 * A_block_size;
        int iColU1_qy = c1 * A_block_size + qyBlock * A_small_block_size;
        int iColQy2_u = c2 * A_block_size + uBlock * A_small_block_size;
        int iColQx2_u = c2 * A_block_size + uBlock * A_small_block_size;

        if (c2 >= 0) {
            for (int m = 0; m < funcCount; m++)
            {
                for (int j = 0; j < A_small_block_size; j++)
                {
                    int iRowU1Current = iRowU1 + m;
                    int iRowU2Current = iRowU2 + m;
                    int iRowQx1Current = iRowQx1 + m;
                    int iRowQx2Current = iRowQx2 + m;
                    int iRowQy1Current = iRowQy1 + m;
                    int iRowQy2Current = iRowQy2 + m;

                    float tmpIntU1_qx = 0.0, tmpIntU1_qy = 0.0;
                    float tmpIntQx2_u = 0.0, tmpIntQy2_u = 0.0;

                    for (int iGP = 0; iGP < edgeGPCount; iGP++)
                    {
                        float cGP1 = edgeWGP[iGP] * baseF(m, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y) * baseF(j, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y);
                        float cGP2 = edgeWGP[iGP] * baseF(m, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y) * baseF(j, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y);

                        float tmpInt1_nx = edgeNormal[i].x * cGP1;
                        float tmpInt1_ny = edgeNormal[i].y * cGP1;

                        float tmpInt2_nx = edgeNormal[i].x * cGP2;
                        float tmpInt2_ny = edgeNormal[i].y * cGP2;

                        tmpIntU1_qx += tmpInt1_nx;

                        tmpIntU1_qy += tmpInt1_ny;

                        tmpIntQx2_u += tmpInt2_nx;

                        tmpIntQy2_u += tmpInt2_ny;
                    }

                    // for u -> qx in row for C1
                    if (abs(tmpIntU1_qx) <= EPS) tmpIntU1_qx = 0.0;

                    tmpIntU1_qx *= edgeJ[i] * TAU;

                    hypreSolver->addMatrElement(iRowU1Current, iColU1_qx + j, -tmpIntU1_qx);

                    // for u -> qx in row for C2
                    hypreSolver->addMatrElement(iRowU2Current, iColU1_qx + j, -tmpIntU1_qx);

                    // for u -> qy in row for C1
                    if (abs(tmpIntU1_qy) <= EPS) tmpIntU1_qy = 0.0;

                    tmpIntU1_qy *= edgeJ[i] * TAU;

                    hypreSolver->addMatrElement(iRowU1Current, iColU1_qy + j, -tmpIntU1_qy);

                    // for u -> qy in row for C2
                    hypreSolver->addMatrElement(iRowU2Current, iColU1_qy + j, -tmpIntU1_qy);

                    // for qx -> u in row for C1
                    if (abs(tmpIntQx2_u) <= EPS) tmpIntQx2_u = 0.0;

                    tmpIntQx2_u *= edgeJ[i];

                    hypreSolver->addMatrElement(iRowQx1Current, iColQx2_u + j, tmpIntQx2_u);

                    // for qx -> u in row for C2
                    hypreSolver->addMatrElement(iRowQx2Current, iColQx2_u + j, tmpIntQx2_u);

                    // for qy -> u in row for C1
                    if (abs(tmpIntQy2_u) <= EPS) tmpIntQy2_u = 0.0;

                    tmpIntQy2_u *= edgeJ[i];

                    hypreSolver->addMatrElement(iRowQy1Current, iColQy2_u + j, tmpIntQy2_u);

                    // for qy -> u in row for C2
                    hypreSolver->addMatrElement(iRowQy2Current, iColQy2_u + j, tmpIntQy2_u);
                }
            }
        }
        else {
            for (int m = 0; m < funcCount; m++)
            {
                for (int j = 0; j < A_small_block_size; j++)
                {
                    int iRowU1Current = iRowU1 + m;

                    float tmpIntU1_qx = 0.0, tmpIntU1_qy = 0.0;

                    for (int iGP = 0; iGP < edgeGPCount; iGP++)
                    {
                        float cGP1 = edgeWGP[iGP] * baseF(m, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y) * baseF(j, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y);

                        float tmpInt1_nx = edgeNormal[i].x * cGP1;
                        float tmpInt1_ny = edgeNormal[i].y * cGP1;

                        tmpIntU1_qx += tmpInt1_nx;
                        tmpIntU1_qy += tmpInt1_ny;
                    }

                    tmpIntU1_qx *= edgeJ[i] * TAU;
                    tmpIntU1_qy *= edgeJ[i] * TAU;

                    hypreSolver->addMatrElement(iRowU1Current, iColU1_qx + j, -tmpIntU1_qx);
                    hypreSolver->addMatrElement(iRowU1Current, iColU1_qy + j, -tmpIntU1_qy);
                }
            }
        }
    }
}

void SetMatrElement(CSRMatrix& matr, int iCell, int iBlock, int iTargetBlock, int iBlockRow, int iBlockCol, float value)
{
    int i = iCell * A_block_size + iBlock * A_small_block_size + iBlockRow;
    int j = iCell * A_block_size + iTargetBlock * A_small_block_size + iBlockCol;

    matr.set(i, j, value);
}

void AddMatrElement(CSRMatrix& matr, int iCell, int iBlock, int iTargetBlock, int iBlockRow, int iBlockCol, float value)
{
    int i = iCell * A_block_size + iBlock * A_small_block_size + iBlockRow;
    int j = iCell * A_block_size + iTargetBlock * A_small_block_size + iBlockCol;

    matr.add(i, j, value);
}

void AddMatrElement_flux(CSRMatrix& matr, int iCell1, int iCell2, int iBlock, int iTargetBlock, int iBlockRow, int iBlockCol, float value)
{
    int i = iCell1 * A_block_size + iBlock * A_small_block_size + iBlockRow;
    int j = iCell2 * A_block_size + iTargetBlock * A_small_block_size + iBlockCol;

    matr.add(i, j, value);
}
void AddFluxMatrElement(CSRMatrix& matr, int iCell1, int iCell2, int iEdge, int iBlock, bool isBorder, int sign)
{
    float tmpInt1 = 0.0;
    float tmpInt2 = 0.0;
    float tmpInt3 = 0.0;
    float tmpInt4 = 0.0;

    if (iBlock == qxBlock)
    {
        for (int m = 0; m < funcCount; m++)
        {
            for (int j = 0; j < A_small_block_size; j++)
            {
                for (int iGP = 0; iGP < edgeGPCount; iGP++)
                {
                    float cGP1 = edgeWGP[iGP] * GetFuncComposition(iCell1, iCell1, m, j, iEdge, iGP);
                    float cGP2 = edgeWGP[iGP] * GetFuncComposition(iCell1, iCell2, m, j, iEdge, iGP);

                    tmpInt1 += cGP1 * edgeNormal[iEdge].x;
                    tmpInt2 += cGP2 * edgeNormal[iEdge].x;
                }

                if (abs(tmpInt1) <= EPS) tmpInt1 = 0.0;
                if (abs(tmpInt2) <= EPS) tmpInt2 = 0.0;

                tmpInt1 *= edgeJ[iEdge] * 0.5 * sign;
                tmpInt2 *= edgeJ[iEdge] * 0.5 * sign;

                AddMatrElement_flux(matr, iCell1, iCell1, iBlock, uBlock, m, j, tmpInt1);
                AddMatrElement_flux(matr, iCell1, iCell2, iBlock, uBlock, m, j, tmpInt2);
            }
        }

    }
    else if (iBlock == qyBlock)
    {
        for (int m = 0; m < funcCount; m++)
        {
            for (int j = 0; j < A_small_block_size; j++)
            {
                for (int iGP = 0; iGP < edgeGPCount; iGP++)
                {
                    float cGP1 = edgeWGP[iGP] * GetFuncComposition(iCell1, iCell1, m, j, iEdge, iGP);
                    float cGP2 = edgeWGP[iGP] * GetFuncComposition(iCell1, iCell2, m, j, iEdge, iGP);

                    tmpInt1 += cGP1 * edgeNormal[iEdge].y;
                    tmpInt2 += cGP2 * edgeNormal[iEdge].y;
                }

                if (abs(tmpInt1) <= EPS) tmpInt1 = 0.0;
                if (abs(tmpInt2) <= EPS) tmpInt2 = 0.0;

                tmpInt1 *= edgeJ[iEdge] * 0.5 * sign;
                tmpInt2 *= edgeJ[iEdge] * 0.5 * sign;

                AddMatrElement_flux(matr, iCell1, iCell1, iBlock, uBlock, m, j, tmpInt1);
                AddMatrElement_flux(matr, iCell1, iCell2, iBlock, uBlock, m, j, tmpInt2);
            }
        }
    }
    else if (iBlock == uBlock)
    {
        if (isBorder) {
            for (int m = 0; m < funcCount; m++)
            {
                for (int j = 0; j < A_small_block_size; j++)
                {
                    for (int iGP = 0; iGP < edgeGPCount; iGP++)
                    {
                        float cGP1 = edgeWGP[iGP] * GetFuncComposition(iCell1, iCell1, m, j, iEdge, iGP);

                        tmpInt1 += cGP1 * edgeNormal[iEdge].x;

                        tmpInt2 += cGP1 * edgeNormal[iEdge].y;
                    }

                    if (abs(tmpInt1) <= EPS) tmpInt1 = 0.0;
                    if (abs(tmpInt2) <= EPS) tmpInt2 = 0.0;

                    tmpInt1 *= edgeJ[iEdge] * TAU * sign;
                    tmpInt2 *= edgeJ[iEdge] * TAU * sign;

                    AddMatrElement_flux(matr, iCell1, iCell1, iBlock, qxBlock, m, j, tmpInt1);
                    AddMatrElement_flux(matr, iCell1, iCell1, iBlock, qyBlock, m, j, tmpInt2);
                }
            }
        }
        else {
            for (int m = 0; m < funcCount; m++)
            {
                for (int j = 0; j < A_small_block_size; j++)
                {
                    for (int iGP = 0; iGP < edgeGPCount; iGP++)
                    {
                        float cGP1 = edgeWGP[iGP] * GetFuncComposition(iCell1, iCell1, m, j, iEdge, iGP);
                        float cGP2 = edgeWGP[iGP] * GetFuncComposition(iCell1, iCell2, m, j, iEdge, iGP);

                        tmpInt1 += cGP1 * edgeNormal[iEdge].x;
                        tmpInt2 += cGP2 * edgeNormal[iEdge].x;

                        tmpInt3 += cGP1 * edgeNormal[iEdge].y;
                        tmpInt4 += cGP2 * edgeNormal[iEdge].y;
                    }

                    if (abs(tmpInt1) <= EPS) tmpInt1 = 0.0;
                    if (abs(tmpInt2) <= EPS) tmpInt2 = 0.0;

                    if (abs(tmpInt3) <= EPS) tmpInt3 = 0.0;
                    if (abs(tmpInt4) <= EPS) tmpInt4 = 0.0;

                    tmpInt1 *= edgeJ[iEdge] * 0.5 * TAU * sign;
                    tmpInt2 *= edgeJ[iEdge] * 0.5 * TAU * sign;

                    tmpInt3 *= edgeJ[iEdge] * 0.5 * TAU * sign;
                    tmpInt4 *= edgeJ[iEdge] * 0.5 * TAU * sign;

                    AddMatrElement_flux(matr, iCell1, iCell1, iBlock, qxBlock, m, j, tmpInt1);
                    AddMatrElement_flux(matr, iCell1, iCell2, iBlock, qxBlock, m, j, tmpInt2);
                    AddMatrElement_flux(matr, iCell1, iCell1, iBlock, qyBlock, m, j, tmpInt3);
                    AddMatrElement_flux(matr, iCell1, iCell2, iBlock, qyBlock, m, j, tmpInt4);
                }
            }
        }
    }
}

float GetFuncComposition(int iCell1, int iCell2, int iFunc1, int iFunc2, int iEdge, int iGP)
{
    return baseF(iFunc1, iCell1, edgeGP[iEdge][iGP].x, edgeGP[iEdge][iGP].y) * baseF(iFunc2, iCell2, edgeGP[iEdge][iGP].x, edgeGP[iEdge][iGP].y);
}

// initialization matrix for implicit method
void initMatrix(CSRMatrix& A)
{
	for (int iCell = 0; iCell < cellsCount; iCell++)
    {
	    for (int iBlock = 0; iBlock < blocksCount; iBlock++)
        {
	        for (int iBlockRow = 0; iBlockRow < A_small_block_size; iBlockRow++)
            {
	            for (int iBlockCol = 0; iBlockCol < A_small_block_size; iBlockCol++)
                {
                    float tmpInt = 0.0;

                    for (int iGP = 0; iGP < cellGPCount; iGP++)
                    {
                        tmpInt += cellWGP[iGP] * baseF(iBlockRow, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]) * baseF(iBlockCol, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                    }

                    tmpInt *= cellJ[iCell];

                    SetMatrElement(A, iCell, iBlock, iBlock, iBlockRow, iBlockCol, tmpInt);

                    if (iBlock == qxBlock)
                    {
                        int iTargetBlock = uBlock;

                        tmpInt = 0.0;

                        for (int iGP = 0; iGP < cellGPCount; iGP++)
                        {
                            float bfDFDx = baseDFDx(iBlockRow, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

                            tmpInt += cellWGP[iGP] * bfDFDx * baseF(iBlockCol, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                        }

                        if (abs(tmpInt) <= EPS) tmpInt = 0.0;

                        tmpInt *= cellJ[iCell];

                        AddMatrElement(A, iCell, iBlock, iTargetBlock, iBlockRow, iBlockCol, tmpInt);
                    }
                    else if (iBlock == qyBlock)
                    {
                        int iTargetBlock = uBlock;

                        tmpInt = 0.0;

                        for (int iGP = 0; iGP < cellGPCount; iGP++)
                        {
                            float bfDFDy = baseDFDy(iBlockRow, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

                            tmpInt += cellWGP[iGP] * bfDFDy * baseF(iBlockCol, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                        }

                        if (abs(tmpInt) <= EPS) tmpInt = 0.0;

                        tmpInt *= cellJ[iCell];

                        AddMatrElement(A, iCell, iBlock, iTargetBlock, iBlockRow, iBlockCol, tmpInt);
                    }
                    else if (iBlock == uBlock)
                    {
                        int iTargetBlock1 = qxBlock;
                        int iTargetBlock2 = qyBlock;

                        float tmpInt1 = 0.0;
                        float tmpInt2 = 0.0;

                        for (int iGP = 0; iGP < cellGPCount; iGP++)
                        {
                            float bfDFDx = baseDFDx(iBlockRow, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                            float bfDFDy = baseDFDy(iBlockRow, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

                            tmpInt1 += cellWGP[iGP] * bfDFDx * baseF(iBlockCol, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                            tmpInt2 += cellWGP[iGP] * bfDFDy * baseF(iBlockCol, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                        }

                        if (abs(tmpInt1) <= EPS) tmpInt1 = 0.0;
                        if (abs(tmpInt2) <= EPS) tmpInt2 = 0.0;

                        tmpInt1 *= cellJ[iCell] * TAU;
                        tmpInt2 *= cellJ[iCell] * TAU;

                        AddMatrElement(A, iCell, iBlock, iTargetBlock1, iBlockRow, iBlockCol, tmpInt1);
                        AddMatrElement(A, iCell, iBlock, iTargetBlock2, iBlockRow, iBlockCol, tmpInt2);
                    }
                }
            }
        }
    }

    // ########################################################################################################################################
    // ################################################## Classic half-sum flux ###############################################################
    // ########################################################################################################################################

    int c1, c2;

    for (int i = 0; i < edgesCount; i++)
    {
        c1 = edgeCell1[i];
        c2 = edgeCell2[i];
        bool isBorder;
        if (c2 >= 0) {
            isBorder = false;

            AddFluxMatrElement(A, c1, c2, i, qxBlock, isBorder, -1);
            AddFluxMatrElement(A, c2, c1, i, qxBlock, isBorder, 1);

            AddFluxMatrElement(A, c1, c2, i, qyBlock, isBorder, -1);
            AddFluxMatrElement(A, c2, c1, i, qyBlock, isBorder, 1);

            AddFluxMatrElement(A, c1, c2, i, uBlock, isBorder, -1);
            AddFluxMatrElement(A, c2, c1, i, uBlock, isBorder, 1);
        }
        else {
            isBorder = true;

            AddFluxMatrElement(A, c1, c1, i, uBlock, isBorder, -1);
        }
    }
}

/*void initMatrix_convection(CSRMatrix& A)
{
    const int qxBlock = 0;
    const int qyBlock = 1;
    const int uBlock = 2;

    A_size = A_block_size * cellsCount;	// 9 * cellsCount

    int iCell = 0;
    int iSmallBlockId = 0;

    // begin matrix initialization
    for (int i = 0; i < A_size; i++)
    {
        if ((i % A_small_block_size) == 0 && (i != 0))
        {
            iSmallBlockId++;
        }

        if ((i % A_block_size) == 0 && (i != 0))
        {
            iCell++;
            iSmallBlockId = 0;
        }

        // variable for current column
        int colId = iCell * A_block_size + iSmallBlockId * A_small_block_size;

        // rowId in each small block
        int rowId = i % A_small_block_size;

        // сalculate the same values of the matrix A
        for (int j = 0; j < A_small_block_size; j++)
        {
            float tmpInt = 0.0;
            A.set(i, colId + j, 0.0);

            for (int iGP = 0; iGP < cellGPCount; iGP++)
            {
                tmpInt += cellWGP[iGP] * baseF(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]) * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
            }

            tmpInt *= cellJ[iCell];

            if (iSmallBlockId == uBlock)
            {
                //tmpInt /= TAU;
            }

            A.set(i, colId + j, tmpInt);
        }

        // calculate vol integral values for qx-block
        if (iSmallBlockId == qxBlock)
        {
            int uColId = colId + uBlock * A_small_block_size;

            for (int j = 0; j < A_small_block_size; j++)
            {
                float tmpInt = 0.0;

                for (int iGP = 0; iGP < cellGPCount; iGP++)
                {
                    float bfDFDx = baseDFDx(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

                    tmpInt += cellWGP[iGP] * bfDFDx * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                }

                if (abs(tmpInt) <= EPS) tmpInt = 0.0;

                tmpInt *= cellJ[iCell];

                A.add(i, uColId + j, tmpInt);
            }
        }
            // calculate vol integral values for qy-block
        else if (iSmallBlockId == qyBlock)
        {
            int uColId = colId + A_small_block_size;

            for (int j = 0; j < A_small_block_size; j++)
            {
                float tmpInt = 0.0;

                for (int iGP = 0; iGP < cellGPCount; iGP++)
                {
                    float bfDFDy = baseDFDy(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

                    tmpInt += cellWGP[iGP] * bfDFDy * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                }

                if (abs(tmpInt) <= EPS) tmpInt = 0.0;

                tmpInt *= cellJ[iCell];

                A.add(i, uColId + j, tmpInt);
            }
        }
            // calculate vol integral values for u-block
        else if (iSmallBlockId == uBlock)
        {
            int qxColId = colId - uBlock * A_small_block_size;
            int qyColId = colId - A_small_block_size;
            int uColId = colId;

            for (int j = 0; j < A_small_block_size; j++)
            {
                float tmpInt1 = 0.0;
                float tmpInt2 = 0.0;

                for (int iGP = 0; iGP < cellGPCount; iGP++)
                {
                    float bfDFDx = baseDFDx(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                    float bfDFDy = baseDFDy(rowId, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);

                    tmpInt1 += cellWGP[iGP] * bfDFDx * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                    tmpInt2 += cellWGP[iGP] * bfDFDy * baseF(j, iCell, cellGPx[iCell * cellGPCount + iGP], cellGPy[iCell * cellGPCount + iGP]);
                }

                if (abs(tmpInt1) <= EPS) tmpInt1 = 0.0;
                if (abs(tmpInt2) <= EPS) tmpInt2 = 0.0;

                tmpInt1 *= cellJ[iCell] * TAU;
                tmpInt2 *= cellJ[iCell] * TAU;

                A.add(i, qxColId + j, tmpInt1);
                A.add(i, qyColId + j, tmpInt2);

                A.add(i, uColId + j, -get_vx(iCell) * tmpInt1);
                A.add(i, uColId + j, -get_vy(iCell) * tmpInt2);
            }
        }
    }

    // ########################################################################################################################################
    // ################################################## Chinese flux ########################################################################
    // ########################################################################################################################################

    int c1, c2;
    for (int i = 0; i < edgesCount; i++)
    {
        c1 = edgeCell1[i];
        c2 = edgeCell2[i];

        int iRowU1 = c1 * A_block_size + 6;
        int iRowU2 = c2 * A_block_size + 6;
        int iRowQy1 = c1 * A_block_size + 3;
        int iRowQy2 = c2 * A_block_size + 3;
        int iRowQx1 = c1 * A_block_size;
        int iRowQx2 = c2 * A_block_size;

        int iColU1_qx = c1 * A_block_size;
        int iColU1_qy = c1 * A_block_size + qyBlock * A_small_block_size;
        int iColQy2_u = c2 * A_block_size + uBlock * A_small_block_size;
        int iColQx2_u = c2 * A_block_size + uBlock * A_small_block_size;

        int iColU1 = c1 * A_block_size + 6;
        int iColU2 = c2 * A_block_size + 6;

        if (c2 >= 0) {
            for (int m = 0; m < funcCount; m++)
            {
                for (int j = 0; j < A_small_block_size; j++)
                {
                    int iRowU1Current = iRowU1 + m;
                    int iRowU2Current = iRowU2 + m;
                    int iRowQx1Current = iRowQx1 + m;
                    int iRowQx2Current = iRowQx2 + m;
                    int iRowQy1Current = iRowQy1 + m;
                    int iRowQy2Current = iRowQy2 + m;

                    float tmpIntU1_qx = 0.0, tmpIntU1_qy = 0.0, tmpIntU1_x = 0.0, tmpIntU2_x = 0.0, tmpIntU1_y = 0.0, tmpIntU2_y = 0.0;
                    float tmpIntU2_qx = 0.0, tmpIntU2_qy = 0.0, tmpIntQx2_u = 0.0, tmpIntQy2_u = 0.0;

                    for (int iGP = 0; iGP < edgeGPCount; iGP++)
                    {
                        float cGP1 = edgeWGP[iGP] * baseF(m, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y) * baseF(j, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y);
                        float cGP2 = edgeWGP[iGP] * baseF(m, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y) * baseF(j, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y);

                        float tmpInt1_nx = edgeNormal[i].x * cGP1;
                        float tmpInt1_ny = edgeNormal[i].y * cGP1;

                        float tmpInt2_nx = edgeNormal[i].x * cGP2;
                        float tmpInt2_ny = edgeNormal[i].y * cGP2;

                        tmpIntU1_qx += tmpInt1_nx;
                        tmpIntU2_qx += tmpInt2_nx;

                        tmpIntU1_x += tmpInt1_nx;
                        tmpIntU2_x += tmpInt2_nx;

                        tmpIntU1_y += tmpInt1_ny;
                        tmpIntU2_y += tmpInt2_ny;

                        tmpIntU1_qy += tmpInt1_ny;
                        tmpIntU2_qy += tmpInt2_ny;

                        tmpIntQx2_u += tmpInt2_nx;

                        tmpIntQy2_u += tmpInt2_ny;
                    }

                    // for u -> u_x in row for C1 (+)
                    if (abs(tmpIntU1_x) <= EPS) tmpIntU1_x = 0.0;
                    if (abs(tmpIntU2_x) <= EPS) tmpIntU2_x = 0.0;

                    tmpIntU1_x *= edgeJ[i] * TAU * 0.5;
                    tmpIntU2_x *= edgeJ[i] * TAU * 0.5;

                    A.add(iRowU1Current, iColU1 + j, tmpIntU1_x * get_vx(c1));
                    A.add(iRowU1Current, iColU1 + j, (get_dx(c1, c2) / TAU) * tmpIntU1_x * get_vx(c1));
                    A.add(iRowU1Current, iColU2 + j, tmpIntU2_x * get_vx(c2));
                    A.add(iRowU1Current, iColU2 + j, -(get_dx(c1, c2) / TAU) * tmpIntU1_x * get_vx(c2));

                    // for u -> u_x in row for C2 (+)
                    A.add(iRowU2Current, iColU1 + j, tmpIntU1_x * get_vx(c1));
                    A.add(iRowU2Current, iColU1 + j, (get_dx(c1, c2) / TAU) * tmpIntU1_x * get_vx(c1));
                    A.add(iRowU2Current, iColU2 + j, tmpIntU2_x * get_vx(c2));
                    A.add(iRowU2Current, iColU2 + j, -(get_dx(c1, c2) / TAU) * tmpIntU2_x * get_vx(c2));

                    // for u -> u_y in row for C1 (+)
                    if (abs(tmpIntU1_y) <= EPS) tmpIntU1_y = 0.0;
                    if (abs(tmpIntU2_y) <= EPS) tmpIntU2_y = 0.0;

                    tmpIntU1_y *= edgeJ[i] * TAU * 0.5;
                    tmpIntU2_y *= edgeJ[i] * TAU * 0.5;

                    A.add(iRowU1Current, iColU1 + j, tmpIntU1_y * get_vy(c1));
                    A.add(iRowU1Current, iColU1 + j, (get_dx(c1, c2) / TAU) * tmpIntU1_y * get_vy(c1));
                    A.add(iRowU1Current, iColU2 + j, tmpIntU2_y * get_vy(c2));
                    A.add(iRowU1Current, iColU2 + j, -(get_dx(c1, c2) / TAU) * tmpIntU2_y * get_vy(c2));

                    // for u -> u_y in row for C2 (+)
                    A.add(iRowU2Current, iColU1 + j, tmpIntU1_y * get_vy(c1));
                    A.add(iRowU2Current, iColU1 + j, (get_dx(c1, c2) / TAU) * tmpIntU1_y * get_vy(c1));
                    A.add(iRowU2Current, iColU2 + j, tmpIntU2_y * get_vy(c2));
                    A.add(iRowU2Current, iColU2 + j, -(get_dx(c1, c2) / TAU) * tmpIntU1_y * get_vy(c2));

                    // for u -> qx in row for C1
                    if (abs(tmpIntU1_qx) <= EPS) tmpIntU1_qx = 0.0;

                    tmpIntU1_qx *= edgeJ[i] * TAU;

                    A.add(iRowU1Current, iColU1_qx + j, -tmpIntU1_qx);

                    // for u -> qx in row for C2
                    A.add(iRowU2Current, iColU1_qx + j, -tmpIntU1_qx);

                    // for u -> qy in row for C1
                    if (abs(tmpIntU1_qy) <= EPS) tmpIntU1_qy = 0.0;

                    tmpIntU1_qy *= edgeJ[i] * TAU;

                    A.add(iRowU1Current, iColU1_qy + j, -tmpIntU1_qy);

                    // for u -> qy in row for C2
                    A.add(iRowU2Current, iColU1_qy + j, -tmpIntU1_qy);

                    // for qx -> u in row for C1
                    if (abs(tmpIntQx2_u) <= EPS) tmpIntQx2_u = 0.0;

                    tmpIntQx2_u *= edgeJ[i];

                    A.add(iRowQx1Current, iColQx2_u + j, tmpIntQx2_u);

                    // for qx -> u in row for C2
                    A.add(iRowQx2Current, iColQx2_u + j, tmpIntQx2_u);

                    // for qy -> u in row for C1
                    if (abs(tmpIntQy2_u) <= EPS) tmpIntQy2_u = 0.0;

                    tmpIntQy2_u *= edgeJ[i];

                    A.add(iRowQy1Current, iColQy2_u + j, tmpIntQy2_u);

                    // for qy -> u in row for C2
                    A.add(iRowQy2Current, iColQy2_u + j, tmpIntQy2_u);
                }
            }
        }
        else {
            for (int m = 0; m < funcCount; m++)
            {
                for (int j = 0; j < A_small_block_size; j++)
                {
                    int iRowU1Current = iRowU1 + m;

                    float tmpIntU1_qx = 0.0, tmpIntU1_qy = 0.0, tmpIntU1_x = 0.0, tmpIntU1_y = 0.0;

                    for (int iGP = 0; iGP < edgeGPCount; iGP++)
                    {
                        float cGP1 = edgeWGP[iGP] * baseF(m, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y) * baseF(j, c1, edgeGP[i][iGP].x, edgeGP[i][iGP].y);
                        float cGP2 = edgeWGP[iGP] * baseF(m, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y) * baseF(j, c2, edgeGP[i][iGP].x, edgeGP[i][iGP].y);

                        float tmpInt1_nx = edgeNormal[i].x * cGP1;
                        float tmpInt1_ny = edgeNormal[i].y * cGP1;

                        tmpIntU1_qx += tmpInt1_nx;

                        tmpIntU1_qy += tmpInt1_ny;

                        tmpIntU1_x += tmpInt1_nx * get_boundary(c1, j);

                        tmpIntU1_y += tmpInt1_ny * get_boundary(c1, j);
                    }

                    // for u -> u_x in row for C1 (+)
                    if (abs(tmpIntU1_x) <= EPS) tmpIntU1_x = 0.0;

                    tmpIntU1_x *= edgeJ[i] * TAU * 0.5;

                    A.add(iRowU1Current, iColU1 + j, tmpIntU1_x * get_vx(c1));
                    A.add(iRowU1Current, iColU1 + j, (get_dx_boundary(c1, j) / TAU) * tmpIntU1_x * get_vx(c1));

                    // for u -> u_y in row for C1 (+)
                    if (abs(tmpIntU1_y) <= EPS) tmpIntU1_y = 0.0;

                    tmpIntU1_y *= edgeJ[i] * TAU * 0.5;

                    A.add(iRowU1Current, iColU1 + j, tmpIntU1_y * get_vy(c1));
                    A.add(iRowU1Current, iColU1 + j, (get_dx(c1, c2) / TAU) * tmpIntU1_y * get_vy(c1));

                    // for u -> qx in row for C1
                    if (abs(tmpIntU1_qx) <= EPS) tmpIntU1_qx = 0.0;

                    tmpIntU1_qx *= edgeJ[i] * TAU;

                    A.add(iRowU1Current, iColU1_qx + j, -tmpIntU1_qx);

                    // for u -> qy in row for C1
                    if (abs(tmpIntU1_qy) <= EPS) tmpIntU1_qy = 0.0;

                    tmpIntU1_qy *= edgeJ[i] * TAU;

                    A.add(iRowU1Current, iColU1_qy + j, -tmpIntU1_qy);
                }
            }
        }
    }

    // ########################################################################################################################################
}*/

float get_vx(int iCell) {
    return 1.0;
}

float get_vy(int iCell) {
    return 1.0;
}

float get_dx(int iCell1, int iCell2) {
    return sqrt(pow_2(cellCx[iCell1] - cellCx[iCell2]) + pow_2(cellCy[iCell1] - cellCy[iCell2]));
}

float get_dx_boundary(int iCell1, int iEdge) {
    float x1 = nodes[edgeNode1[iEdge]].x;
    float y1 = nodes[edgeNode1[iEdge]].y;

    float x2 = nodes[edgeNode2[iEdge]].x;
    float y2 = nodes[edgeNode2[iEdge]].y;

    float a = y2 - y1;
    float b = -(x2 - x1);
    float c = y1 * (x2 - x1) - x1 * (y2 - y1);

    float x0 = cellCx[iCell1];
    float y0 = cellCy[iCell1];

    return (abs(a * x0 + b * y0 + c) / sqrt(pow_2(a) + pow_2(b)));
}

float get_boundary(int iCell, int iEdge) {
    return 0.0;
}