#include <iostream>
#include <cmath>

double X;
double T;
double tau, h;

int nodesCount;
int intervalCount;
int funcCount;
double *nodes;
double *nodesC;

double *u;
double *q;

double **f_u;
double **f_q;
double **invA, **B;

int fileCounter = 0;
int trueFileCounter = 0;

double baseFunction(int intervalIndex, int functionIndex, double x)
{
    switch (functionIndex)
    {
        case 0:
            return 1.0;
        case 1:
            return (x - nodesC[intervalIndex]) / h;
    }
}

double leftBoundary(double t)
{
    return 0.0;
}

double rightBoundary(double t)
{
    return 0.0;
}

double trueSolution(double x, double t)
{
    return exp(-M_PI * M_PI * t) * sin(M_PI * x);
}

void printFile()
{
    char fileName[50];
    sprintf(fileName, "output_%d.txt", fileCounter++);

    FILE* outputFile = fopen(fileName, "w");

    for (int i = 0; i < nodesCount; i++) {
        fprintf(outputFile, "%f %f\n", i * h, u[i]);
    }

    fclose(outputFile);
}

void printTrueSolution()
{
    char fileName[50];
    sprintf(fileName, "trueOutput_%d.txt", trueFileCounter++);

    FILE* outputFile = fopen(fileName, "w");

    for (int i = 0; i < nodesCount; i++) {
        fprintf(outputFile, "%f %f\n", i * h, u[i]);
    }

    fclose(outputFile);
}

double calcNorm(double t)
{
    double norm = 0;

    for (int i = 0; i < nodesCount; i++) {
        norm += (u[i] - trueSolution(i * h, t));
    }

    return sqrt(norm);
}

void initData()
{
    nodesCount = 501;
    intervalCount = nodesCount - 1;
    funcCount = 2;

    T = 1.0;
    X = 1.0;
    h = X / (intervalCount);
    tau = 0.1 * h * h;

    nodes = new double[nodesCount];
    u = new double[nodesCount];
    q = new double[nodesCount];

    nodesC = new double[intervalCount];

    f_u = new double*[intervalCount];
    f_q = new double*[intervalCount];

    for (int i = 0; i < intervalCount; i++) {
        f_u[i] = new double[funcCount];
        f_q[i] = new double[funcCount];
    }

    invA = new double*[funcCount];
    for (int i = 0; i < funcCount; i++) {
        invA[i] = new double[funcCount];
    }

    B = new double*[intervalCount];
    for(int i = 0; i < intervalCount; i++) {
        B[i] = new double[funcCount];
    }

    for (int i = 0; i < intervalCount; i++) {
        nodesC[i] = (nodes[i] + nodes[i + 1]) / 2.0;
    }

    invA[0][0] = 1.0 / h;
    invA[0][1] = 0.0;
    invA[1][0] = 0.0;
    invA[1][1] = 12.0 / h;

    u[0] = leftBoundary(0.0);

    for (int i = 1; i < nodesCount - 1; i++) {
        u[i] = sin(M_PI * i * h);
    }

    u[nodesCount - 1] = rightBoundary(0.0);
}

void destroyData()
{
    delete [] nodes;
    delete [] u;
    delete [] q;

    delete [] nodesC;

    for (int i = 0; i < intervalCount; i++) {
        delete [] f_u[i];
        delete [] f_q[i];
    }

    delete [] f_u;
    delete [] f_q;

    for (int i = 0; i < funcCount; i++) {
        delete [] invA[i];
    }

    delete [] invA;

    for(int i = 0; i < intervalCount; i++) {
        delete [] B[i];
    }

    delete [] B;
}

int main()
{
    double t = 0.0;
    int iter = 0;
    double stepT = 0.1;

    initData();
    printFile();

    while (t < T) {
        iter++;
        t += tau;

        for (int i = 0; i < nodesCount - 1; i++) {
            B[i][0] = u[i + 1] - u[i];
            B[i][1] = 0.0;
        }

        for (int i = 0; i < intervalCount; i++) {
            f_q[i][0] = invA[0][0] * B[i][0] + invA[0][1] * B[i][1];
            f_q[i][1] = invA[1][0] * B[i][0] + invA[1][1] * B[i][1];
        }

        q[0] = f_q[0][0] * baseFunction(0, 0, h) + f_q[0][1] * baseFunction(0, 1, h);

        for (int i = 1; i < nodesCount - 1; i++) {
            auto q_leftNode = f_q[i - 1][0] * baseFunction(i - 1, 0, i * h) + f_q[i - 1][1] * baseFunction(i - 1, 1, i * h);
            auto q_RightNode = f_q[i][0] * baseFunction(i, 0, i * h) + f_q[i][1] * baseFunction(i, 1, i * h);

            q[i] = (q_leftNode + q_RightNode) * 0.5;
        }

        q[nodesCount - 1] = f_q[nodesCount - 2][0] * baseFunction(nodesCount - 2, 0, (nodesCount - 1) * h) + f_q[nodesCount - 2][1] * baseFunction(nodesCount - 2, 1, (nodesCount - 1) * h);

        for (int i = 0; i < intervalCount; i++) {
            B[i][0] = q[i + 1] - q[i];
            B[i][1] = (q[i + 1] + q[i]) * 0.5 - f_q[i][0];
        }

        for (int i = 0; i < intervalCount; i++) {
            f_u[i][0] = invA[0][0] * B[i][0] + invA[0][1] * B[i][1];
            f_u[i][1] = invA[1][0] * B[i][0] + invA[1][1] * B[i][1];
        }

        u[0] = leftBoundary(t);

        for (int i = 0; i < nodesCount - 1; i++) {
            auto u_leftNode = f_u[i - 1][0] * baseFunction(i - 1, 0, i * h) + f_u[i - 1][1] * baseFunction(i - 1, 1, i * h);
            auto u_RightNode = f_u[i][0] * baseFunction(i, 0, i * h) + f_u[i][1] * baseFunction(i, 1, i * h);

            u[i] += 0.5 * tau * (u_RightNode + u_leftNode);
        }

        if((int(stepT / tau) == iter)) {
            printFile();
            stepT += 0.1;

            auto norm = calcNorm(t);
            printf("********* t = %f ***********\n norm = %f\n", t, norm);
        }
    }

    t = 0.0;
    stepT = 0.0;
    iter = 0;

    while (t < T) {
        if((int(stepT / tau) == iter)) {
            for (int i = 0; i < nodesCount; i++) {
                u[i] = trueSolution(i * h, t);
            }

            printTrueSolution();
            stepT += 0.1;
        }

        t += tau;
        iter++;
    }

    destroyData();
}